var last_menu_item = null;

function on_ajax_success(data, textStatus, jqXHR) {
    if (data != "") {
        alert(data);
    }
    if (last_menu_item != null) {
        $(last_menu_item).trigger('click'); // Reload the request div in order to see changes
    }
}

function on_ajax_error() {
    alert('Error');
}

function do_transact_ajax(url, data) {
    $.ajax({
        url: url,
        type: 'POST',
        data: data,
        success: on_ajax_success,
        error: on_ajax_error
    });
}

function ref_get_drivers_by_transport() {
    $('#get-drivers-by-transport-result').html('');
    var table = $('.select-transport-table:visible'),
        index = table[0].dataset.selectindex;
    var data = {
        transportid: index == -1 ? -1 : $(table[0].rows[index].cells[0]).html(),
    };
    $.ajax({
        url: 'requests/ref/get_drivers_by_transport.php',
        type: 'POST',
        data: data,
        success: function(data, textStatus, jqXHR) {
            $('#get-drivers-by-transport-result').html(data);
        },
        error: on_ajax_error
    });
}

function ref_get_kilometrage() {
    $('#get-kilometrage-result').html('');
    var table = $('.select-transport-table:visible'),
        index = table[0].dataset.selectindex;
    var data = {
        typeid: $('#select-transport-type').val().split('-')[2],
        transportid: index == -1 ? -1 : $(table[0].rows[index].cells[0]).html(),
        start: $('#get-kilometrage-start').val(),
        end: $('#get-kilometrage-end').val()
    };
    $.ajax({
        url: 'requests/ref/get_kilometrage.php',
        type: 'POST',
        data: data,
        success: function(data, textStatus, jqXHR) {
            $('#get-kilometrage-result').html(data);
        },
        error: on_ajax_error
    });
}

function ref_get_work_by_transport() {
    $('#get-work-result').html('');
    var table = $('.select-transport-table:visible'),
        index = table[0].dataset.selectindex;
    if (index == -1) {
        return;
    }
    var data = {
        transportid: $(table[0].rows[index].cells[0]).html()
    };
    $.ajax({
        url: 'requests/ref/get_work_by_transport.php',
        type: 'POST',
        data: data,
        success: function(data, textStatus, jqXHR) {
            $('#get-work-result').html(data);
        },
        error: on_ajax_error
    });
}

function ref_get_transportations() {
    $('#get-transportations-result').html('');
    var table = $('.select-transport-table:visible'),
        index = table[0].dataset.selectindex;
    var data = {
        transportid: index == -1 ? -1 : $(table[0].rows[index].cells[0]).html(),
        start: $('#get-transportations-start').val(),
        end: $('#get-transportations-end').val()
    };
    $.ajax({
        url: 'requests/ref/get_transportations.php',
        type: 'POST',
        data: data,
        success: function(data, textStatus, jqXHR) {
            $('#get-transportations-result').html(data);
        },
        error: on_ajax_error
    });
}

function ref_get_parts_count() {
    $('#get-parts-count-result').html('');
    var table = $('.select-transport-table:visible'),
        index = table[0].dataset.selectindex;
    var data = {
        transportid: index == -1 ? -1 : $(table[0].rows[index].cells[0]).html(),
        partid: $('#select-transport-part').val().split("-")[2]
    };
    $.ajax({
        url: 'requests/ref/get_parts_count.php',
        type: 'POST',
        data: data,
        success: function(data, textStatus, jqXHR) {
            $('#get-parts-count-result').html(data);
        },
        error: on_ajax_error
    });
}

function ref_get_adopted_decommissioned_transport(e) {
    $('#get-adopted-decommissioned-transport-result').html('');
    var senderid = $(e.target).attr('id'),
        url = senderid == 'btn-get-adopted-transport' ? 'requests/ref/get_adopted_transport.php' : 'requests/ref/get_decommissioned_transport.php';
        data = {
        start: $('#get-adopted-decommissioned-start').val(),
        end: $('#get-adopted-decommissioned-end').val()
    },

    $.ajax({
        url: url,
        type: 'POST',
        data: data,
        success: function(data, textStatus, jqXHR) {
            $('#get-adopted-decommissioned-transport-result').html(data);
        },
        error: on_ajax_error
    });
}

function ref_get_brigadier_subordinates() {
    $('#get-brigadier-subordinates-result').html('');
    var brigadiertable = $('#select-brigadier-table'),
        brigadierindex = brigadiertable[0].dataset.selectindex;
    if (brigadierindex == -1) {
        return;
    }
    var data = {
        brigadierid: $(brigadiertable[0].rows[brigadierindex].cells[0]).html()
    };
    $.ajax({
        url: 'requests/ref/get_brigadier_subordinates.php',
        type: 'POST',
        data: data,
        success: function(data, textStatus, jqXHR) {
            $('#get-brigadier-subordinates-result').html(data);
        },
        error: on_ajax_error
    });
}

function ref_get_master_subordinates() {
    $('#get-master-subordinates-result').html('');
    var mastertable = $('#select-master-table'),
        masterindex = mastertable[0].dataset.selectindex;
    if (masterindex == -1) {
        return;
    }
    var data = {
        masterid: $(mastertable[0].rows[masterindex].cells[0]).html()
    };
    $.ajax({
        url: 'requests/ref/get_master_subordinates.php',
        type: 'POST',
        data: data,
        success: function(data, textStatus, jqXHR) {
            $('#get-master-subordinates-result').html(data);
        },
        error: on_ajax_error
    });
}

function ref_get_work_by_worker() {
    $('#get-work-by-worker-result').html('');
    var table = $('.select-worker-table:visible'),
        index = table[0].dataset.selectindex;
    if (index == -1) {
        return;
    }
    var data = {
        workerid: $(table[0].rows[index].cells[0]).html()
    };
    $.ajax({
        url: 'requests/ref/get_work_by_worker.php',
        type: 'POST',
        data: data,
        success: function(data, textStatus, jqXHR) {
            $('#get-work-by-worker-result').html(data);
        },
        error: on_ajax_error
    });
}

function transact_add_transport() {
    var data = {
        typeid: $('#add-transport-type').val().split('-')[2],
        marque: $('#add-transport-marque').val(),
        date: $('#add-transport-date').val()
    };
    if (data.date == '') {
        return;
    }
    $('.transport-attribute:visible').each(function(k, element) {
        element = $(element);
        var attrid = element.attr('id').split('-')[2];
        data[attrid] = element.val();
    });
    do_transact_ajax('requests/transact/add_transport.php', data);
}

function transact_remove_transport() {
    var table = $('.select-transport-table:visible'),
        index = table[0].dataset.selectindex;
    if (index == -1) {
        return;
    }
    var data = {
        transportid: $(table[0].rows[index].cells[0]).html(),
        date: $('#remove-transport-date').val()
    };
    if (data.date == '') {
        return;
    }
    do_transact_ajax('requests/transact/remove_transport.php', data);
}

function transact_add_worker() {
    var data = {
        typeid: $('#add-worker-type').val().split('-')[2],
        name: $('#add-worker-name').val(),
        birthdate: $('#add-worker-birthdate').val(),
    };
    $('.worker-attribute:visible').each(function(k, element) {
        element = $(element);
        var attrid = element.attr('id').split('-')[2];
        data[attrid] = element.val();
    });
    do_transact_ajax('requests/transact/add_worker.php', data);
}

function transact_remove_worker() {
    var table = $('.select-worker-table:visible'),
        index = table[0].dataset.selectindex;
    if (index == -1) {
        return;
    }
    var data = {
        workerid: $(table[0].rows[index].cells[0]).html()
    };
    do_transact_ajax('requests/transact/remove_worker.php', data);
}

function transact_add_driver() {
    var data = {
        name: $('#add-driver-name').val(),
        birthdate: $('#add-driver-birthdate').val(),
        category: $('#add-driver-category').val()
    };
    do_transact_ajax('requests/transact/add_driver.php', data);
}

function transact_remove_driver() {
    var table = $('#select-driver-table'),
        index = table[0].dataset.selectindex;
    if (index == -1) {
        return;
    }
    var data = {
        driverid: $(table[0].rows[index].cells[0]).html()
    };
    do_transact_ajax('requests/transact/remove_driver.php', data);
}

function transact_set_driver() {
    var transporttable = $('.select-transport-table:visible'),
        drivertable = $('#select-driver-table'),
        transportindex = transporttable[0].dataset.selectindex,
        driverindex = drivertable[0].dataset.selectindex;
    if (transportindex == -1 || driverindex == -1) {
        return;
    }
    var data = {
        transportid: $(transporttable[0].rows[transportindex].cells[0]).html(),
        driverid: $(drivertable[0].rows[driverindex].cells[0]).html()
    };
    do_transact_ajax('requests/transact/set_driver.php', data);
}

function transact_add_route() {
    var data = {
        name: $('#add-route-name').val(),
        start: $('#add-route-start').val(),
        end: $('#add-route-end').val(),
    };
    do_transact_ajax('requests/transact/add_route.php', data);
}

function transact_remove_route() {
    var table = $('#select-route-table'),
        index = table[0].dataset.selectindex;
    if (index == -1) {
        return;
    }
    var data = {
        routeid: $(table[0].rows[index].cells[0]).html()
    };
    do_transact_ajax('requests/transact/remove_route.php', data);
}

function transact_add_brigadier() {
    var data = {
        name: $('#add-brigadier-name').val(),
        birthdate: $('#add-brigadier-birthdate').val()
    };
    do_transact_ajax('requests/transact/add_brigadier.php', data);
}

function transact_add_master() {
    var data = {
        name: $('#add-master-name').val(),
        birthdate: $('#add-master-birthdate').val()
    };
    do_transact_ajax('requests/transact/add_master.php', data);
}

function transact_set_master_for_brigadier() {
    var mastertable = $('#select-master-table'),
        brigadiertable = $('#select-brigadier-table'),
        masterindex = mastertable[0].dataset.selectindex,
        brigadierindex = brigadiertable[0].dataset.selectindex;
    if (masterindex == -1 || brigadierindex == -1) {
        return;
    }
    var data = {
        masterid: $(mastertable[0].rows[masterindex].cells[0]).html(),
        brigadierid: $(brigadiertable[0].rows[brigadierindex].cells[0]).html()
    };
    do_transact_ajax('requests/transact/set_master_for_brigadier.php', data);
}

function transact_set_brigadier_for_worker() {
    var brigadiertable = $('#select-brigadier-table'),
        workertable = $('#select-worker-or-driver-table'),
        brigadierindex = brigadiertable[0].dataset.selectindex,
        workerindex = workertable[0].dataset.selectindex;
    if (brigadierindex == -1 || workerindex == -1) {
        return;
    }
    var data = {
        brigadierid: $(brigadiertable[0].rows[brigadierindex].cells[0]).html(),
        workerid: $(workertable[0].rows[workerindex].cells[0]).html()
    };
    do_transact_ajax('requests/transact/set_brigadier_for_worker.php', data);
}

function transact_set_route() {
    var transporttable = $('.select-transport-table:visible'),
        routetable = $('#select-route-table'),
        transportindex = transporttable[0].dataset.selectindex,
        routeindex = routetable[0].dataset.selectindex;
    if (transportindex == -1 || routeindex == -1) {
        return;
    }
    var data = {
        transportid: $(transporttable[0].rows[transportindex].cells[0]).html(),
        routeid: $(routetable[0].rows[routeindex].cells[0]).html()
    };
    do_transact_ajax('requests/transact/set_route.php', data);
}

function transact_add_garage_property() {
    var data = {
        typeid: $('#select-garage-property-type').val().split("-")[3],
        description: $('#add-garage-property-description').val()
    };
    do_transact_ajax('requests/transact/add_garage_property.php', data);
}

function transact_add_transportation() {
    var table = $('.select-transport-table:visible'),
        index = table[0].dataset.selectindex;
    if (index == -1) {
        return;
    }
    var data = {
        transportid: $(table[0].rows[index].cells[0]).html(),
        description: $('#add-transportation-description').val(),
        distance: $('#add-transportation-distance').val(),
        start: $('#add-transportation-start').val(),
        end: $('#add-transportation-end').val()
    };
    do_transact_ajax('requests/transact/add_transportation.php', data);
}

function transact_add_kilometrage() {
    var table = $('.select-transport-table:visible'),
        index = table[0].dataset.selectindex;
    if (index == -1) {
        return;
    }
    var data = {
        transportid: $(table[0].rows[index].cells[0]).html(),
        distance: $('#add-kilometrage-distance').val(),
        start: $('#add-kilometrage-start').val(),
        end: $('#add-kilometrage-end').val()
    };
    do_transact_ajax('requests/transact/add_kilometrage.php', data);
}

function transact_add_work() {
    var transporttable = $('.select-transport-table:visible'),
        transportindex = transporttable[0].dataset.selectindex;
        workertable = $('.select-worker-table:visible'),
        workerindex = workertable[0].dataset.selectindex;
    if (transportindex == -1 || workerindex == -1) {
        return;
    }
    var data = {
        workerid: $(workertable[0].rows[workerindex].cells[0]).html(),
        transportid: $(transporttable[0].rows[transportindex].cells[0]).html(),
        partid: $('#select-transport-part').val().split("-")[2],
        description: $('#add-work-description').val(),
        start: $('#add-work-start').val(),
        end: $('#add-work-end').val(),
        cost: $('#add-work-cost').val()
    };

    do_transact_ajax('requests/transact/add_work.php', data);
}

function plan_assign_transport() {
    var data = {
        typeid: $('#select-transport-type').val().split('-')[2]
    };

    do_transact_ajax('requests/plan/assign_transport.php', data);
}

function bind_menu_item(menuselector, url, btnselector, btncallback) {
    $(menuselector).click(function(e) {
        e.preventDefault();
        last_menu_item = e.target;
        $('.requestdiv').remove();
        $.ajax({
            url: url,
            success: function(data, textStatus, jqXHR) {
                $('#container').html(data);
                $(".selectpicker").selectpicker();
                if (btnselector != null && btncallback != null) {
                    $(btnselector).click(btncallback);
                }
            }
        });
    });
}

$(document).ready(function() {
    // Bind menu items
    bind_menu_item('#menu-get-transport', 'ajaxforms/ref_get_transport.php', null, null);
    bind_menu_item('#menu-get-drivers-by-transport', 'ajaxforms/ref_get_drivers_by_transport.php', '#btn-get-drivers-by-transport', ref_get_drivers_by_transport);
    bind_menu_item('#menu-get-drivers-distribution', 'ajaxforms/ref_get_drivers_distribution.php', null, null);
    bind_menu_item('#menu-get-transport-distribution', 'ajaxforms/ref_get_transport_distribution.php', null, null);
    bind_menu_item('#menu-get-work-by-transport', 'ajaxforms/ref_get_work_by_transport.php', '#btn-get-work', ref_get_work_by_transport);
    bind_menu_item('#menu-get-garage-property', 'ajaxforms/ref_get_garage_property.php', null, null);
    bind_menu_item('#menu-get-kilometrage', 'ajaxforms/ref_get_kilometrage.php', '#btn-get-kilometrage', ref_get_kilometrage);
    bind_menu_item('#menu-get-transportations', 'ajaxforms/ref_get_transportations.php', '#btn-get-transportations', ref_get_transportations);
    bind_menu_item('#menu-get-parts-count', 'ajaxforms/ref_get_parts_count.php', '#btn-get-parts-count', ref_get_parts_count);
    bind_menu_item('#menu-get-adopted-decommissioned-transport', 'ajaxforms/ref_get_adopted_decommissioned.php', '.btn-get-adopted-decommissioned-transport', ref_get_adopted_decommissioned_transport);
    bind_menu_item('#menu-get-brigadier-subordinates', 'ajaxforms/ref_get_brigadier_subordinates.php', '#btn-get-brigadier-subordinates', ref_get_brigadier_subordinates);
    bind_menu_item('#menu-get-master-subordinates', 'ajaxforms/ref_get_master_subordinates.php', '#btn-get-master-subordinates', ref_get_master_subordinates);

    bind_menu_item('#menu-get-work-by-worker', 'ajaxforms/ref_get_work_by_worker.php', '#btn-get-work-by-worker', ref_get_work_by_worker);

    bind_menu_item('#menu-add-transport', 'ajaxforms/transact_add_transport.php', '#btn-add-transport', transact_add_transport);
    bind_menu_item('#menu-remove-transport', 'ajaxforms/transact_remove_transport.php', '#btn-remove-transport', transact_remove_transport);
    bind_menu_item('#menu-add-worker', 'ajaxforms/transact_add_worker.php', '#btn-add-worker', transact_add_worker);
    bind_menu_item('#menu-remove-worker', 'ajaxforms/transact_remove_worker.php', '#btn-remove-worker', transact_remove_worker);
    bind_menu_item('#menu-add-driver', 'ajaxforms/transact_add_driver.php', '#btn-add-driver', transact_add_driver);
    bind_menu_item('#menu-remove-driver', 'ajaxforms/transact_remove_driver.php', '#btn-remove-driver', transact_remove_driver);
    bind_menu_item('#menu-set-driver', 'ajaxforms/transact_set_driver.php', '#btn-set-driver', transact_set_driver);
    bind_menu_item('#menu-add-route', 'ajaxforms/transact_add_route.php', '#btn-add-route', transact_add_route);
    bind_menu_item('#menu-remove-route', 'ajaxforms/transact_remove_route.php', '#btn-remove-route', transact_remove_route);
    bind_menu_item('#menu-add-brigadier', 'ajaxforms/transact_add_brigadier.php', '#btn-add-brigadier', transact_add_brigadier);
    bind_menu_item('#menu-add-master', 'ajaxforms/transact_add_master.php', '#btn-add-master', transact_add_master);
    bind_menu_item('#menu-set-master-for-brigadier', 'ajaxforms/transact_set_master_for_brigadier.php', '#btn-set-master-for-brigadier', transact_set_master_for_brigadier);
    bind_menu_item('#menu-set-brigadier-for-worker', 'ajaxforms/transact_set_brigadier_for_worker.php', '#btn-set-brigadier-for-worker', transact_set_brigadier_for_worker);
    bind_menu_item('#menu-set-route', 'ajaxforms/transact_set_route.php', '#btn-set-route', transact_set_route);
    bind_menu_item('#menu-add-garage-property', 'ajaxforms/transact_add_garage_property.php', '#btn-add-garage-property', transact_add_garage_property);
    bind_menu_item('#menu-add-transportation', 'ajaxforms/transact_add_transportation.php', '#btn-add-transportation', transact_add_transportation);
    bind_menu_item('#menu-add-kilometrage', 'ajaxforms/transact_add_kilometrage.php', '#btn-add-kilometrage', transact_add_kilometrage);
    bind_menu_item('#menu-add-work', 'ajaxforms/transact_add_work.php', '#btn-add-work', transact_add_work);

    bind_menu_item('#menu-assign-transport', 'ajaxforms/plan_assign_transport.php', '#btn-assign-transport', plan_assign_transport);
});
