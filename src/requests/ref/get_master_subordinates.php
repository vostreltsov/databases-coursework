<?php

$rootdir = dirname(dirname(dirname(__FILE__)));

require_once($rootdir . '/config.php');
require_once($rootdir . '/dbfuncs.php');
require_once($rootdir . '/htmlfuncs.php');
require_once($rootdir . '/requests/common.php');

check_post_params_existance(array('masterid'));

// Connect to the database
$mysqli = db_connect();

// Get everything needed from the request
$masterid = $_POST['masterid'];

$subordinates = db_get_master_subordinates($masterid);
$tableid = "get-master-subordinates-result-table";
$result = html_for_table($tableid, 'table table-hover', false, $subordinates);

echo $result;
