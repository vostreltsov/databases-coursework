<?php

$rootdir = dirname(dirname(dirname(__FILE__)));

require_once($rootdir . '/config.php');
require_once($rootdir . '/dbfuncs.php');
require_once($rootdir . '/htmlfuncs.php');
require_once($rootdir . '/requests/common.php');

check_post_params_existance(array('workerid'));

// Connect to the database
$mysqli = db_connect();

// Get everything needed from the request
$workerid = $_POST['workerid'];

$tableid = "get-work-by-worker-result-table";

$result = html_for_table($tableid, 'table table-hover', false, db_get_work_by_worker($workerid));

echo $result;
