<?php

$rootdir = dirname(dirname(dirname(__FILE__)));

require_once($rootdir . '/config.php');
require_once($rootdir . '/dbfuncs.php');
require_once($rootdir . '/requests/common.php');

check_post_params_existance(array('transportid', 'partid'));

// Connect to the database
$mysqli = db_connect();

// Get everything needed from the request
$transportid = $_POST['transportid'];
$partid = $_POST['partid'];

$query = "SELECT COUNT(*) FROM work WHERE transport_part_type_id = $partid";
if ($transportid != -1) {
    $query .= " AND transport_id = $transportid";
}

// Insert kilometrage
$result = db_ajax_query($mysqli, $query);
$result = $result->fetch_array(MYSQLI_NUM);
$result = $result[0];
if (!$result) {
    $result = 0;
}

echo $result . ' шт.';
