<?php

$rootdir = dirname(dirname(dirname(__FILE__)));

require_once($rootdir . '/config.php');
require_once($rootdir . '/dbfuncs.php');
require_once($rootdir . '/htmlfuncs.php');
require_once($rootdir . '/requests/common.php');

check_post_params_existance(array('transportid', 'start', 'end'));

// Connect to the database
$mysqli = db_connect();

// Get everything needed from the request
$transportid = $_POST['transportid'];
$start = $_POST['start'];
$end = $_POST['end'];


$transportations = db_get_transportations($transportid, $start, $end);
$tableid = "get-transportations-result-table";
$result = html_for_table($tableid, 'table table-hover', false, $transportations);

echo $result;
