<?php

$rootdir = dirname(dirname(dirname(__FILE__)));

require_once($rootdir . '/config.php');
require_once($rootdir . '/dbfuncs.php');
require_once($rootdir . '/requests/common.php');

check_post_params_existance(array('typeid', 'transportid', 'start', 'end'));

// Connect to the database
$mysqli = db_connect();

// Get everything needed from the request
$typeid = $_POST['typeid'];
$transportid = $_POST['transportid'];
$start = $_POST['start'];
$end = $_POST['end'];

$query = "SELECT SUM(distance) FROM
          (kilometrage INNER JOIN transport ON transport_id = transport.id AND transport_type_id = $typeid)
          WHERE date_start >= '$start' AND date_end <= '$end'";
if ($transportid != -1) {
    $query .= " AND transport_id = $transportid";
}

// Insert kilometrage
$result = db_ajax_query($mysqli, $query);
$result = $result->fetch_array(MYSQLI_NUM);
$result = $result[0];
if (!$result) {
    $result = 0;
}

echo $result . ' километров';
