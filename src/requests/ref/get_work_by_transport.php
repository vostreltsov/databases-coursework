<?php

$rootdir = dirname(dirname(dirname(__FILE__)));

require_once($rootdir . '/config.php');
require_once($rootdir . '/dbfuncs.php');
require_once($rootdir . '/htmlfuncs.php');
require_once($rootdir . '/requests/common.php');

check_post_params_existance(array('transportid'));

// Connect to the database
$mysqli = db_connect();

// Get everything needed from the request
$transportid = $_POST['transportid'];

$tableid = "get-work-result-table";

$result = html_for_table($tableid, 'table table-hover', false, db_get_work_by_transport($transportid));

$query = "SELECT SUM(cost) FROM detailed_work WHERE transport_id = $transportid";
$total = db_ajax_query($mysqli, $query);
$total = $total->fetch_array(MYSQLI_NUM);
$total = $total[0];
if (!$total) {
    $total = 0;
}

$result .= 'Итого: ' . $total;

echo $result;
