<?php

$rootdir = dirname(dirname(dirname(__FILE__)));

require_once($rootdir . '/config.php');
require_once($rootdir . '/dbfuncs.php');
require_once($rootdir . '/htmlfuncs.php');
require_once($rootdir . '/requests/common.php');

check_post_params_existance(array('brigadierid'));

// Connect to the database
$mysqli = db_connect();

// Get everything needed from the request
$brigadierid = $_POST['brigadierid'];

$subordinates = db_get_brigadier_subordinates($brigadierid);
$tableid = "get-brigadier-subordinates-result-table";
$result = html_for_table($tableid, 'table table-hover', false, $subordinates);

echo $result;
