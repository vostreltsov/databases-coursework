<?php

$rootdir = dirname(dirname(dirname(__FILE__)));

require_once($rootdir . '/config.php');
require_once($rootdir . '/dbfuncs.php');
require_once($rootdir . '/htmlfuncs.php');
require_once($rootdir . '/requests/common.php');

check_post_params_existance(array('start', 'end'));

// Connect to the database
$mysqli = db_connect();

// Get everything needed from the request
$start = $_POST['start'];
$end = $_POST['end'];

$result = 'Списанный транспорт';
foreach (db_get_transport_types() as $typeid => $typename) {
    $tableid = "adopted-transport-table-$typeid";
    $transport = db_get_all_transport_by_type($typeid, null, array('start' => $start, 'end' => $end));
    $table = html_for_table($tableid, 'table table-hover', false, $transport);
    $result .= '<h2>' . $typename . '</h2>' . $table . '<spacer>';
}

echo $result;
