<?php

$rootdir = dirname(dirname(dirname(__FILE__)));

require_once($rootdir . '/config.php');
require_once($rootdir . '/htmlfuncs.php');
require_once($rootdir . '/dbfuncs.php');
require_once($rootdir . '/requests/common.php');

check_post_params_existance(array('transportid'));

// Connect to the database
$mysqli = db_connect();

// Get everything needed from the request
$transportid = $_POST['transportid'];

// TODO: создать представление для водителя?
$tableid = "driver-table";
$drivers = db_get_all_drivers($transportid);
$table = html_for_table($tableid, 'table table-hover', false, $drivers);

echo $table;
