<?php

function check_post_params_existance($params) {
    foreach ($params as $param) {
        if (!array_key_exists($param, $_POST)) {
            exit('Invalid POST request');
        }
    }
}
