<?php

$rootdir = dirname(dirname(dirname(__FILE__)));

require_once($rootdir . '/config.php');
require_once($rootdir . '/dbfuncs.php');
require_once($rootdir . '/requests/common.php');

$needed = array('typeid', 'marque', 'date');

check_post_params_existance($needed);

// Connect to the database
$mysqli = db_connect();

// Get everything needed from the request
$typeid = (int)$_POST['typeid'];
$marque = $_POST['marque'];
$date = $_POST['date'];
$attributes = array();
foreach ($_POST as $attrid => $attrvalue) {
    if (!in_array($attrid, $needed)) {
        $attributes[(int)$attrid] = $attrvalue;
    }
}

// Insert transport
db_ajax_query($mysqli, "INSERT INTO transport(transport_type_id, marque, date_adopted) VALUES($typeid, '$marque', '$date')");

$transportid = $mysqli->insert_id;

// Insert attributes
foreach ($attributes as $attrid => $attrvalue) {
    db_ajax_query($mysqli, "INSERT INTO transport_attribute(value, transport_type_attribute_id, transport_id) VALUES('$attrvalue', $attrid, $transportid)");
}

echo 'OK';
