<?php

$rootdir = dirname(dirname(dirname(__FILE__)));

require_once($rootdir . '/config.php');
require_once($rootdir . '/dbfuncs.php');
require_once($rootdir . '/requests/common.php');

$needed = array('name', 'start', 'end');

check_post_params_existance($needed);

// Connect to the database
$mysqli = db_connect();

// Get everything needed from the request
$name = $_POST['name'];
$start = $_POST['start'];
$end = $_POST['end'];

// Insert route
db_ajax_query($mysqli, "INSERT INTO route(name, start, end) VALUES('$name', '$start', '$end')");

echo 'OK';
