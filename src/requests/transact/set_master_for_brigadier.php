<?php

$rootdir = dirname(dirname(dirname(__FILE__)));

require_once($rootdir . '/config.php');
require_once($rootdir . '/dbfuncs.php');
require_once($rootdir . '/requests/common.php');

check_post_params_existance(array('masterid', 'brigadierid'));

// Connect to the database
$mysqli = db_connect();

// Get everything needed from the request
$masterid = (int)$_POST['masterid'];
$brigadierid = (int)$_POST['brigadierid'];

// Update brigadier record
db_ajax_query($mysqli, "UPDATE brigadier SET master_id = $masterid WHERE id = $brigadierid");

echo 'OK';
