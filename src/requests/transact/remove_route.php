<?php

$rootdir = dirname(dirname(dirname(__FILE__)));

require_once($rootdir . '/config.php');
require_once($rootdir . '/dbfuncs.php');
require_once($rootdir . '/requests/common.php');

check_post_params_existance(array('routeid'));

// Connect to the database
$mysqli = db_connect();

// Get everything needed from the request
$routeid = (int)$_POST['routeid'];

// Remove route
db_ajax_query($mysqli, "DELETE FROM route WHERE id = $routeid");

echo 'OK';
