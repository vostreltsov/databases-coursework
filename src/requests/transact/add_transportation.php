<?php

$rootdir = dirname(dirname(dirname(__FILE__)));

require_once($rootdir . '/config.php');
require_once($rootdir . '/dbfuncs.php');
require_once($rootdir . '/requests/common.php');

$needed = array('transportid', 'description', 'distance', 'start', 'end');

check_post_params_existance($needed);

if (!ctype_digit($_POST['distance'])) {
    exit('Invalid distance');
}

// Connect to the database
$mysqli = db_connect();

// Get everything needed from the request
$transportid = $_POST['transportid'];
$description = $_POST['description'];
$distance = (int)$_POST['distance'];
$start = $_POST['start'];
$end = $_POST['end'];

if ($distance <= 0) {
    exit('Distance should be greater than 0');
}

// Insert kilometrage
db_ajax_query($mysqli, "INSERT INTO kilometrage(transport_id, distance, date_start, date_end) VALUES($transportid, $distance, '$start', '$end')");

$kilometrageid = $mysqli->insert_id;

// Insert transportation
db_ajax_query($mysqli, "INSERT INTO transportation(description, kilometrage_id) VALUES('$description', $kilometrageid)");

echo 'OK';
