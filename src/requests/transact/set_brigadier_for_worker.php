<?php

$rootdir = dirname(dirname(dirname(__FILE__)));

require_once($rootdir . '/config.php');
require_once($rootdir . '/dbfuncs.php');
require_once($rootdir . '/requests/common.php');

check_post_params_existance(array('brigadierid', 'workerid'));

// Connect to the database
$mysqli = db_connect();

// Get everything needed from the request
$brigadierid = (int)$_POST['brigadierid'];
$workerid = (int)$_POST['workerid'];

// Update brigadier record
db_ajax_query($mysqli, "UPDATE worker SET brigadier_id = $brigadierid WHERE id = $workerid");

echo 'OK';
