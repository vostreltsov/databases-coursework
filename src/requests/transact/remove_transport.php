<?php

$rootdir = dirname(dirname(dirname(__FILE__)));

require_once($rootdir . '/config.php');
require_once($rootdir . '/dbfuncs.php');
require_once($rootdir . '/requests/common.php');

check_post_params_existance(array('transportid', 'date'));

// Connect to the database
$mysqli = db_connect();

// Get everything needed from the request
$transportid = (int)$_POST['transportid'];
$date = $_POST['date'];

// Set transport decommission date
db_ajax_query($mysqli, "UPDATE transport SET date_decommissioned = '$date' WHERE id = $transportid");

echo 'OK';
