<?php

$rootdir = dirname(dirname(dirname(__FILE__)));

require_once($rootdir . '/config.php');
require_once($rootdir . '/dbfuncs.php');
require_once($rootdir . '/requests/common.php');

$needed = array('name', 'birthdate', 'typeid');

check_post_params_existance($needed);

// Connect to the database
$mysqli = db_connect();

// Get everything needed from the request
$name = $_POST['name'];
$birthdate = $_POST['birthdate'];
$typeid = $_POST['typeid'];
$attributes = array();
foreach ($_POST as $attrid => $attrvalue) {
    if (!in_array($attrid, $needed)) {
        $attributes[(int)$attrid] = $attrvalue;
    }
}

// Insert worker
db_ajax_query($mysqli, "INSERT INTO worker(name, birth_date, worker_type_id) VALUES('$name', '$birthdate', '$typeid')");

$workerid = $mysqli->insert_id;

// Insert attributes
foreach ($attributes as $attrid => $attrvalue) {
    db_ajax_query($mysqli, "INSERT INTO worker_attribute(value, worker_type_attribute_id, worker_id) VALUES('$attrvalue', $attrid, $workerid)");
}

echo 'OK';
