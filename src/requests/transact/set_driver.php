<?php

$rootdir = dirname(dirname(dirname(__FILE__)));

require_once($rootdir . '/config.php');
require_once($rootdir . '/dbfuncs.php');
require_once($rootdir . '/requests/common.php');

check_post_params_existance(array('transportid', 'driverid'));

// Connect to the database
$mysqli = db_connect();

// Get everything needed from the request
$transportid = (int)$_POST['transportid'];
$driverid = (int)$_POST['driverid'];

// Update transport record
db_ajax_query($mysqli, "UPDATE driver SET transport_id = $transportid WHERE id = $driverid");

echo 'OK';
