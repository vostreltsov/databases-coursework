<?php

$rootdir = dirname(dirname(dirname(__FILE__)));

require_once($rootdir . '/config.php');
require_once($rootdir . '/dbfuncs.php');
require_once($rootdir . '/requests/common.php');

check_post_params_existance(array('workerid'));

// Connect to the database
$mysqli = db_connect();

// Get everything needed from the request
$workerid = (int)$_POST['workerid'];

// Remove worker
db_ajax_query($mysqli, "DELETE FROM worker WHERE id = $workerid");

echo 'OK';
