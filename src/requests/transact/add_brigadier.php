<?php

$rootdir = dirname(dirname(dirname(__FILE__)));

require_once($rootdir . '/config.php');
require_once($rootdir . '/dbfuncs.php');
require_once($rootdir . '/requests/common.php');

$needed = array('name', 'birthdate');

check_post_params_existance($needed);

// Connect to the database
$mysqli = db_connect();

// Get everything needed from the request
$name = $_POST['name'];
$birthdate = $_POST['birthdate'];

// Insert worker
db_ajax_query($mysqli, "INSERT INTO worker(name, birth_date) VALUES('$name', '$birthdate')");

$workerid = $mysqli->insert_id;

// Insert brigadier
db_ajax_query($mysqli, "INSERT INTO brigadier(worker_id) VALUES($workerid)");

echo 'OK';
