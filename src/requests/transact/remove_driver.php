<?php

$rootdir = dirname(dirname(dirname(__FILE__)));

require_once($rootdir . '/config.php');
require_once($rootdir . '/dbfuncs.php');
require_once($rootdir . '/requests/common.php');

check_post_params_existance(array('driverid'));

// Connect to the database
$mysqli = db_connect();

// Get everything needed from the request
$driverid = (int)$_POST['driverid'];

// Remove worker, driver will be removed too
db_ajax_query($mysqli, "DELETE worker FROM (worker INNER JOIN driver ON worker_id = worker.id) WHERE driver.id = $driverid");

echo 'OK';
