<?php

$rootdir = dirname(dirname(dirname(__FILE__)));

require_once($rootdir . '/config.php');
require_once($rootdir . '/dbfuncs.php');
require_once($rootdir . '/requests/common.php');

$needed = array('workerid', 'transportid', 'partid', 'description', 'start', 'end', 'cost');

check_post_params_existance($needed);

// Connect to the database
$mysqli = db_connect();

// Get everything needed from the request
$workerid = $_POST['workerid'];
$transportid = (int)$_POST['transportid'];
$partid = (int)$_POST['partid'];
$description = (int)$_POST['description'];
$start = $_POST['start'];
$end = $_POST['end'];
$cost = $_POST['cost'];

// Insert work
db_ajax_query($mysqli, "INSERT INTO work(description, date_start, date_end, cost, worker_id, transport_part_type_id, transport_id) VALUES('$description', '$start', '$end', '$cost', $workerid, $partid, $transportid)");

echo 'OK';
