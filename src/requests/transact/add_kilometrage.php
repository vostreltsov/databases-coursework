<?php

$rootdir = dirname(dirname(dirname(__FILE__)));

require_once($rootdir . '/config.php');
require_once($rootdir . '/dbfuncs.php');
require_once($rootdir . '/requests/common.php');

$needed = array('transportid', 'distance', 'start', 'end');

check_post_params_existance($needed);

if (!ctype_digit($_POST['distance'])) {
    exit('Invalid distance');
}

// Connect to the database
$mysqli = db_connect();

// Get everything needed from the request
$transportid = $_POST['transportid'];
$distance = (int)$_POST['distance'];
$start = $_POST['start'];
$end = $_POST['end'];

if ($distance <= 0) {
    exit('Distance should be greater than 0');
}

// Insert kilometrage
db_ajax_query($mysqli, "INSERT INTO kilometrage(transport_id, distance, date_start, date_end) VALUES($transportid, $distance, '$start', '$end')");

echo 'OK';
