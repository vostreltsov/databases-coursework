<?php

$rootdir = dirname(dirname(dirname(__FILE__)));

require_once($rootdir . '/config.php');
require_once($rootdir . '/dbfuncs.php');
require_once($rootdir . '/requests/common.php');

$needed = array('name', 'birthdate', 'category');

check_post_params_existance($needed);

// Connect to the database
$mysqli = db_connect();

// Get everything needed from the request
$name = $_POST['name'];
$birthdate = $_POST['birthdate'];
$category = $_POST['category'];

// Insert worker
db_ajax_query($mysqli, "INSERT INTO worker(name, birth_date) VALUES('$name', '$birthdate')");

$workerid = $mysqli->insert_id;

// Insert driver
db_ajax_query($mysqli, "INSERT INTO driver(worker_id, category) VALUES($workerid, '$category')");

echo 'OK';
