<?php

$rootdir = dirname(dirname(dirname(__FILE__)));

require_once($rootdir . '/config.php');
require_once($rootdir . '/dbfuncs.php');
require_once($rootdir . '/requests/common.php');

$needed = array('typeid', 'description');

check_post_params_existance($needed);

// Connect to the database
$mysqli = db_connect();

// Get everything needed from the request
$typeid = (int)$_POST['typeid'];
$description = $_POST['description'];

// Insert garage property
db_ajax_query($mysqli, "INSERT INTO garage_property(description, garage_property_type_id) VALUES('$description', $typeid)");

echo 'OK';
