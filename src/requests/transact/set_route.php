<?php

$rootdir = dirname(dirname(dirname(__FILE__)));

require_once($rootdir . '/config.php');
require_once($rootdir . '/dbfuncs.php');
require_once($rootdir . '/requests/common.php');

check_post_params_existance(array('transportid', 'routeid'));

// Connect to the database
$mysqli = db_connect();

// Get everything needed from the request
$transportid = (int)$_POST['transportid'];
$routeid = (int)$_POST['routeid'];

// Update transport record
db_ajax_query($mysqli, "UPDATE transport SET route_id = $routeid WHERE id = $transportid");

echo 'OK';
