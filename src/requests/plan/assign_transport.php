<?php

$rootdir = dirname(dirname(dirname(__FILE__)));

require_once($rootdir . '/config.php');
require_once($rootdir . '/dbfuncs.php');
require_once($rootdir . '/requests/common.php');

$needed = array('typeid');

check_post_params_existance($needed);

// Connect to the database
$mysqli = db_connect();

// Get everything needed from the request
$typeid = $_POST['typeid'];

// Assign transport
db_ajax_query($mysqli, "CALL ASSIGN_TRANSPORT_TO_ROUTES('$typeid')");

echo 'OK';
