<?php

$rootdir = dirname(__FILE__);

function html_header($title = null) {
    $result = '<html lang="en">' .
              '<head>' .
              '  <meta charset="utf-8">' .
              '  <meta name="viewport" content="width=device-width, initial-scale=1.0">' .
              '  <link href="css/bootstrap.min.css" rel="stylesheet">' .
              '  <link href="css/bootstrap-select.min.css" rel="stylesheet">' .
              '  <link href="css/styles.css" rel="stylesheet">' .
              '  <script src="js/jquery-2.0.3.min.js"></script>' .
              '  <script src="js/bootstrap.min.js"></script>' .
              '  <script src="js/bootstrap-select.min.js"></script>' .
              '  <script src="js/script.js"></script>';

    if ($title) {
        $result .= '  <title>' . $title . '</title>';
    }

    $result .= '</head>' .
               '<body>';
    return $result;
}

function html_footer() {
    $result =
              '</body>' .
              '</html>';
    return $result;
}

function script_for_clickable_row($tableid, $addtags) {
    $result = "$('#$tableid').on('click', 'tbody tr', function(event) {
                   $(this).addClass('active').siblings().removeClass('active');
               });";
    return $addtags ? "<script type='text/javascript'>" . $result . "</script>"
                    : $result;
}

function html_for_table($tableid, $tableclass, $clickable, $data) {
    $onclick = $clickable
             ? "$(\"#$tableid\")[0].dataset.selectindex=this.rowIndex"
             : '';
    $header = array_shift($data);
    $table = "<table id='$tableid' data-selectindex='-1' class='$tableclass'><thead><tr>";
    foreach ($header as $column) {
        $table .= '<th>' . $column . '</th>';
    }
    $table .= '</tr></thead><tbody>';
    foreach ($data as $row) {
        $table .= "<tr onclick='$onclick'>";
        foreach ($row as $col) {
            $table .= "<td>$col</td>";
        }
        $table .= '</tr>';
    }
    $table .= '</tbody></table>';
    return $table;
}

function html_for_transport_part_type_selection() {
    global $rootdir;
    require_once($rootdir . '/dbfuncs.php');

    $result = '<select id="select-transport-part" class="spacer selectpicker width100">';
    foreach (db_get_transport_part_types() as $id => $name) {
        $result .= "<option value='select-part-$id'>" . $name . '</option>';
    }
    return $result;
}

function html_for_transport_type_selection($only_allowed_type_names = null) {
    global $rootdir;
    require_once($rootdir . '/dbfuncs.php');

    $types = '<select id="select-transport-type" class="spacer selectpicker width100">';
    foreach (db_get_transport_types() as $typeid => $typename) {
        if ($only_allowed_type_names !== null && !in_array($typename, $only_allowed_type_names)) {
            continue;
        }
        $types .= "<option value='select-transport-$typeid'>" . $typename . '</option>';
    }
    $types .= '</select>';
    return $types;
}

function html_for_garage_property_type_selection() {
    global $rootdir;
    require_once($rootdir . '/dbfuncs.php');

    $types = '<select id="select-garage-property-type" class="spacer selectpicker width100">';
    foreach (db_get_garage_property_types() as $typeid => $typename) {
        if ($only_allowed_type_names !== null && !in_array($typename, $only_allowed_type_names)) {
            continue;
        }
        $types .= "<option value='select-garage-property-$typeid'>" . $typename . '</option>';
    }
    $types .= '</select>';
    return $types;
}

function html_for_transport_selection($only_allowed_type_names = null) {
    global $rootdir;
    require_once($rootdir . '/dbfuncs.php');

    $tables = '';
    $script = '<script type="text/javascript">
                   $("#select-transport-type").change(function(e) {
                       $(".select-transport-table").hide();
                       var typeid = $("#select-transport-type").val().split("-")[2];';

    foreach (db_get_transport_types() as $typeid => $typename) {
        if ($only_allowed_type_names !== null && !in_array($typename, $only_allowed_type_names)) {
            continue;
        }
        $tableid = "select-transport-table-$typeid";

        $script .= "\n                       if (typeid == $typeid) {" .
                   "\n                           $('#$tableid').show();" .
                   "\n                       }\n";
        $script .= script_for_clickable_row($tableid, false);

        $transport = db_get_all_transport_by_type($typeid);
        $tables .= html_for_table($tableid, 'table table-hover select-transport-table', true, $transport);
    }
    $script .= "\n});$('#select-transport-type').trigger('change');</script>";
    return html_for_transport_type_selection($only_allowed_type_names) . $tables . $script;
}

function html_for_worker_selection() {
    global $rootdir;
    require_once($rootdir . '/dbfuncs.php');

    $types = '<select id="select-worker-type" class="spacer selectpicker width100">';
    $tables = '';
    $script = '<script type="text/javascript">
                   $("#select-worker-type").change(function(e) {
                       $(".select-worker-table").hide();
                       var typeid = $("#select-worker-type").val().split("-")[2];';

    foreach (db_get_worker_types() as $typeid => $typename) {
        $tableid = "select-worker-table-$typeid";

        $types .= "<option value='select-worker-$typeid'>" . $typename . '</option>';
        $script .= "\n                       if (typeid == $typeid) {" .
                   "\n                           $('#$tableid').show();" .
                   "\n                       }\n";
        $script .= script_for_clickable_row($tableid, false);

        $workers = db_get_all_workers($typeid);
        $tables .= html_for_table($tableid, 'table table-hover select-worker-table', true, $workers);
    }
    $types .= '</select>';
    $script .= "\n});$('#select-worker-type').trigger('change');</script>";

    return $types . $tables . $script;
}

function html_for_driver_selection() {
    global $rootdir;
    require_once($rootdir . '/dbfuncs.php');

    $tableid = "select-driver-table";
    $drivers = db_get_all_drivers();
    $table = html_for_table($tableid, 'table table-hover', true, $drivers);
    $script = script_for_clickable_row($tableid, true);
    return $table . $script;
}

function html_for_brigadier_selection() {
    global $rootdir;
    require_once($rootdir . '/dbfuncs.php');

    $tableid = "select-brigadier-table";
    $brigadiers = db_get_all_brigadiers();
    $table = html_for_table($tableid, 'table table-hover', true, $brigadiers);
    $script = script_for_clickable_row($tableid, true);
    return $table . $script;
}

function html_for_master_selection() {
    global $rootdir;
    require_once($rootdir . '/dbfuncs.php');

    $tableid = "select-master-table";
    $masters = db_get_all_masters();
    $table = html_for_table($tableid, 'table table-hover', true, $masters);
    $script = script_for_clickable_row($tableid, true);
    return $table . $script;
}

function html_for_worker_or_driver_selection() {
    global $rootdir;
    require_once($rootdir . '/dbfuncs.php');

    $tableid = "select-worker-or-driver-table";
    $brigadiers = db_get_all_workers_and_drivers();
    $table = html_for_table($tableid, 'table table-hover', true, $brigadiers);
    $script = script_for_clickable_row($tableid, true);
    return $table . $script;
}

function html_for_route_selection() {
    global $rootdir;
    require_once($rootdir . '/dbfuncs.php');

    $tableid = "select-route-table";
    $routes = db_get_all_routes();
    $table = html_for_table($tableid, 'table table-hover', true, $routes);
    $script = script_for_clickable_row($tableid, true);
    return $table . $script;
}
