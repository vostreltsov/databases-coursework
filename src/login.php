<?php
    session_start();

    if (isset($_REQUEST['user']) && isset($_REQUEST['password'])) {
        $_SESSION['user'] = $_REQUEST['user'];
        $_SESSION['password'] = $_REQUEST['password'];
        header('Location: index.php');
    } else {
        header('Location: loginform.php');
    }
