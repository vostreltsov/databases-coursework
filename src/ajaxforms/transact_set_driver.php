<?php

$rootdir = dirname(dirname(__FILE__));

require_once($rootdir . '/htmlfuncs.php');

echo '<div id="set-driver" class="requestdiv col-lg-6">' .
     '<h3>Транспортное средство</h3>' . html_for_transport_selection() .
     '<h3>Водитель</h3>' . html_for_driver_selection() .
     '<button id="btn-set-driver" class="btn btn-primary col-lg-12 spacer">Закрепить водителя за транспортом</button>' .
     '</div>';
