<?php

$rootdir = dirname(dirname(__FILE__));

require_once($rootdir . '/dbfuncs.php');

$types = '<select id="add-worker-type" class="spacer selectpicker width100">';
$attributes = '';
$script = '<script type="text/javascript">
               $("#add-worker-type").change(function(e) {
                   $(".worker-attribute").hide();
                   var typeid = $("#add-worker-type").val().split("-")[2];';

foreach (db_get_worker_types() as $typeid => $typename) {
    $types .= "<option value='add-worker-$typeid'>" . $typename . '</option>';
    $script .= "\n                       if (typeid == $typeid) {\n";
    foreach (db_get_worker_type_attributes($typeid) as $attrid => $attrname) {
        $attrid = "add-worker-$attrid";
        $attributes .= "<input id='$attrid' type='text' placeholder='$attrname' class='form-control worker-attribute spacer'/>";
        $script .= "                           $('#$attrid').show();";
    }
    $script .= "\n                       }";
}
$types .= '</select>';
$script .= "\n});$('#add-worker-type').trigger('change');</script>";

echo '<div id="add-worker" class="requestdiv col-lg-6">' .
     $types .
     '<input id="add-worker-name" type="text" placeholder="ФИО" class="form-control spacer"/>' .
     '<input id="add-worker-birthdate" type="date" value="1990-12-31" class="form-control col-lg-6 spacer"/>' .
     $attributes .
     $script .
     '<button id="btn-add-worker" class="btn btn-primary col-lg-12 spacer">Добавить работника</button>' .
     '</div>' .
     "<script type='text/javascript'>" .
     '$("#add-worker-birthdate").val(new Date().toJSON().slice(0,10));' .
     "</script>";

