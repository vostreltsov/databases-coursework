<?php

$rootdir = dirname(dirname(__FILE__));

require_once($rootdir . '/htmlfuncs.php');

echo '<div id="set-brigadier-for-worker" class="requestdiv col-lg-6">' .
     '<h3>Бригадир</h3>' .
     html_for_brigadier_selection() .
     '<h3>Рабочий</h3>' .
     html_for_worker_or_driver_selection() .
     '<button id="btn-set-brigadier-for-worker" class="btn btn-primary col-lg-12 spacer">Подчинить рабочего бригадиру</button>' .
     '</div>';
