<?php

$rootdir = dirname(dirname(__FILE__));

require_once($rootdir . '/htmlfuncs.php');
require_once($rootdir . '/dbfuncs.php');

echo 'Если не выделено транспортное средство, запросится пробег для категории транспорта в целом' .
     '<div id="get-kilometrage" class="requestdiv col-lg-12">' .
     html_for_transport_selection() .
     '<p class="spacer">Начало:</p><input id="get-kilometrage-start" type="date" class="form-control col-lg-6 spacer"/>' .
     '<p class="spacer">Конец:</p><input id="get-kilometrage-end" type="date" class="form-control col-lg-6 spacer"/>' .
     '<button id="btn-get-kilometrage" class="btn btn-primary col-lg-12 spacer">Получить данные о пробеге</button>' .
     '<div id="get-kilometrage-result">' . '</div>' .
     '</div>' .
     "<script type='text/javascript'>" .
     '$("#get-kilometrage-start").val(new Date().toJSON().slice(0,10));' .
     '$("#get-kilometrage-end").val(new Date().toJSON().slice(0,10));' .
     "</script>";
