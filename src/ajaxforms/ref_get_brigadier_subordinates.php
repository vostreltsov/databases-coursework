<?php

$rootdir = dirname(dirname(__FILE__));

require_once($rootdir . '/htmlfuncs.php');

echo '<div id="get-brigadier-subordinates" class="requestdiv col-lg-6">' .
     '<h3>Бригадир</h3>' .
     html_for_brigadier_selection() .
     '<button id="btn-get-brigadier-subordinates" class="btn btn-primary col-lg-12 spacer">Получить рабочих, подчиненных бригадиру</button>' .
     '<div id="get-brigadier-subordinates-result">' . '</div>' .
     '</div>';
