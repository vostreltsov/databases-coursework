<?php

$rootdir = dirname(dirname(__FILE__));

require_once($rootdir . '/htmlfuncs.php');
require_once($rootdir . '/dbfuncs.php');

echo '<div id="get-work-by-worker" class="requestdiv col-lg-12">' .
     '<h3>Рабочий</h3>' . html_for_worker_selection() .
     '<button id="btn-get-work-by-worker" class="btn btn-primary col-lg-12 spacer">Получить данные о работе</button>' .
     '<div id="get-work-by-worker-result">' . '</div>' .
     '</div>';

