<?php

$rootdir = dirname(dirname(__FILE__));

require_once($rootdir . '/htmlfuncs.php');

echo '<div id="get-master-subordinates" class="requestdiv col-lg-6">' .
     '<h3>Мастер</h3>' .
     html_for_master_selection() .
     '<button id="btn-get-master-subordinates" class="btn btn-primary col-lg-12 spacer">Получить бригадиров, подчиненных мастеру</button>' .
     '<div id="get-master-subordinates-result">' . '</div>' .
     '</div>';
