<?php

$rootdir = dirname(dirname(__FILE__));

require_once($rootdir . '/htmlfuncs.php');

echo '<div id="add-work" class="requestdiv col-lg-6">' .
     '<h3>Рабочий</h3>' . html_for_worker_selection() .
     '<h3>Транспорт</h3>' . html_for_transport_selection() .
     '<h3>Отремонтированная деталь</h3>' . html_for_transport_part_type_selection() .
     '<input id="add-work-description" type="text" placeholder="Описание выполненной работы" class="form-control spacer"/>' .
     '<h3>Начало работы</h3><input id="add-work-start" type="date" class="form-control col-lg-6 spacer"/>' .
     '<h3>Конец работы</h3><input id="add-work-end" type="date" class="form-control col-lg-6 spacer"/>' .
     '<input id="add-work-cost" type="text" placeholder="Затраты на работу" class="form-control spacer"/>' .
     '<button id="btn-add-work" class="btn btn-primary col-lg-12 spacer">Добавить выполненную работу</button>' .
     '</div>' .
     "<script type='text/javascript'>" .
     '$("#add-work-start").val(new Date().toJSON().slice(0,10));' .
     '$("#add-work-end").val(new Date().toJSON().slice(0,10));' .
     "</script>";
