<?php

$rootdir = dirname(dirname(__FILE__));

require_once($rootdir . '/htmlfuncs.php');

echo '<div id="remove-worker" class="requestdiv col-lg-6">' .
     html_for_worker_selection() .
     '<button id="btn-remove-worker" class="btn btn-primary col-lg-12 spacer">Удалить работника</button>' .
     '</div>';
