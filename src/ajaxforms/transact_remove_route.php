<?php

$rootdir = dirname(dirname(__FILE__));

require_once($rootdir . '/htmlfuncs.php');

echo '<div id="remove-route" class="requestdiv col-lg-6">' .
     html_for_route_selection() .
     '<button id="btn-remove-route" class="btn btn-primary col-lg-12 spacer">Удалить маршрут</button>' .
     '</div>';
