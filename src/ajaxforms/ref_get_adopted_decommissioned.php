<?php

$rootdir = dirname(dirname(__FILE__));

require_once($rootdir . '/htmlfuncs.php');
require_once($rootdir . '/dbfuncs.php');

echo '<div id="remove-transport" class="requestdiv col-lg-12">' .
     '<p class="spacer">Начало:</p><input id="get-adopted-decommissioned-start" type="date" class="form-control col-lg-6 spacer"/>' .
     '<p class="spacer">Конец:</p><input id="get-adopted-decommissioned-end" type="date" class="form-control col-lg-6 spacer"/>' .
     '<div class="btn-group col-lg-12">' .
     '<button id="btn-get-adopted-transport" class="btn-get-adopted-decommissioned-transport btn btn-primary col-lg-5 spacer">Получить данные о полученном транспорте</button>' .
     '<button id="btn-get-decommissioned-transport" class="btn-get-adopted-decommissioned-transport btn btn-primary col-lg-5 spacer pull-right">Получить данные о списанном транспорте</button>' .
     '</div>' .
     '<div id="get-adopted-decommissioned-transport-result">' .
     '</div>' .
     "<script type='text/javascript'>" .
     '$("#get-adopted-decommissioned-start").val(new Date().toJSON().slice(0,10));' .
     '$("#get-adopted-decommissioned-end").val(new Date().toJSON().slice(0,10));' .
     "</script>";
