<?php

$rootdir = dirname(dirname(__FILE__));

require_once($rootdir . '/htmlfuncs.php');

echo '<div id="set-route" class="requestdiv col-lg-12">' .
     '<h3>Тип транспортного средства</h3>' . html_for_transport_type_selection(array('Автобус', 'Маршрутное такси')) .
     '<button id="btn-assign-transport" class="btn btn-primary col-lg-12 spacer">Распределить транспорт по маршрутам</button>' .
     '</div>';
