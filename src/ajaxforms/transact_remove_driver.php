<?php

$rootdir = dirname(dirname(__FILE__));

require_once($rootdir . '/htmlfuncs.php');

echo '<div id="remove-driver" class="requestdiv col-lg-6">' .
     html_for_driver_selection() .
     '<button id="btn-remove-driver" class="btn btn-primary col-lg-12 spacer">Удалить водителя</button>' .
     '</div>';
