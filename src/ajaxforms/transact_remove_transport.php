<?php

$rootdir = dirname(dirname(__FILE__));

require_once($rootdir . '/htmlfuncs.php');

echo '<div id="remove-transport" class="requestdiv col-lg-6">' .
     html_for_transport_selection() .
     '<p class="spacer">Дата списания:</p><input id="remove-transport-date" type="date" class="form-control col-lg-6 spacer"/>' .
     '<button id="btn-remove-transport" class="btn btn-primary col-lg-12 spacer">Списать транспорт</button>' .
     '</div>' .
     "<script type='text/javascript'>" .
     '$("#remove-transport-date").val(new Date().toJSON().slice(0,10));' .
     "</script>";
