<?php

$rootdir = dirname(dirname(__FILE__));

require_once($rootdir . '/htmlfuncs.php');
require_once($rootdir . '/dbfuncs.php');

$tableid = "get-drivers-distribution-table";
$transport = db_get_all_transport_by_type($typeid);

echo
'<div id="get-drivers-distribution" class="requestdiv col-lg-12">' .
    html_for_table($tableid, 'table table-hover', false, db_get_drivers_distribution()) .
'</div>';
