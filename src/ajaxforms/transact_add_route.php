<?php

$rootdir = dirname(dirname(__FILE__));

require_once($rootdir . '/htmlfuncs.php');

echo '<div id="add-route" class="requestdiv col-lg-6">' .
     '<input id="add-route-name" type="text" placeholder="Название маршрута" class="form-control spacer"/>' .
     '<input id="add-route-start" type="text" placeholder="Начальная точка" class="form-control spacer"/>' .
     '<input id="add-route-end" type="text" placeholder="Конечная точка" class="form-control spacer"/>' .
     '<button id="btn-add-route" class="btn btn-primary col-lg-12 spacer">Добавить маршрут</button>' .
     '</div>';
