<?php

$rootdir = dirname(dirname(__FILE__));

require_once($rootdir . '/htmlfuncs.php');

echo '<div id="set-master-for-brigadier" class="requestdiv col-lg-6">' .
     '<h3>Мастер</h3>' .
     html_for_master_selection() .
     '<h3>Бригадир</h3>' .
     html_for_brigadier_selection() .
     '<button id="btn-set-master-for-brigadier" class="btn btn-primary col-lg-12 spacer">Подчинить бригадира мастеру</button>' .
     '</div>';
