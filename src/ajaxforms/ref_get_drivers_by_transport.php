<?php

$rootdir = dirname(dirname(__FILE__));

require_once($rootdir . '/htmlfuncs.php');
require_once($rootdir . '/dbfuncs.php');

echo 'Если не выделено транспортное средство, запросится список всех водителей' .
'<div id="get-drivers-by-transport" class="requestdiv col-lg-12">' .

     html_for_transport_selection() .
     '<button id="btn-get-drivers-by-transport" class="btn btn-primary col-lg-12 spacer">Получить список водителей</button>' .
     '</div>' .
     '<div id="get-drivers-by-transport-result">';
