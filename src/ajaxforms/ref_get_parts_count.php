<?php

$rootdir = dirname(dirname(__FILE__));

require_once($rootdir . '/htmlfuncs.php');
require_once($rootdir . '/dbfuncs.php');

echo 'Если не выделено транспортное средство, запросится статистика в целом' .
     '<div id="get-parts-count" class="requestdiv col-lg-12">' .
     html_for_transport_selection() .
     html_for_transport_part_type_selection() .
     '<input style="visibility:hidden"/>' .  // DO NOT TOUCH THIS
     '<button id="btn-get-parts-count" class="btn btn-primary col-lg-12 spacer">Получить количество использованных деталей</button>' .
     '<div id="get-parts-count-result">' . '</div>' .
     '</div>';
