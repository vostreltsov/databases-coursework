<?php

$rootdir = dirname(dirname(__FILE__));

require_once($rootdir . '/htmlfuncs.php');

echo '<div id="add-driver" class="requestdiv col-lg-6">' .
     '<input id="add-driver-name" type="text" placeholder="ФИО" class="form-control spacer"/>' .
     '<input id="add-driver-birthdate" type="date" value="1990-12-31" class="form-control col-lg-6 spacer"/>' .
     '<input id="add-driver-category" type="text" placeholder="Категория" class="form-control spacer"/>' .
     '<button id="btn-add-driver" class="btn btn-primary col-lg-12 spacer">Добавить водителя</button>' .
     '</div>' .
     "<script type='text/javascript'>" .
     '$("#add-driver-birthdate").val(new Date().toJSON().slice(0,10));' .
     "</script>";
