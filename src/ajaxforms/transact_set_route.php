<?php

$rootdir = dirname(dirname(__FILE__));

require_once($rootdir . '/htmlfuncs.php');

echo '<div id="set-route" class="requestdiv col-lg-6">' .
     '<h3>Транспортное средство</h3>' . html_for_transport_selection(array('Автобус', 'Маршрутное такси')) .
     '<h3>Маршрут</h3>' . html_for_route_selection() .
     '<button id="btn-set-route" class="btn btn-primary col-lg-12 spacer">Закрепить автомобиль за маршрутом</button>' .
     '</div>';
