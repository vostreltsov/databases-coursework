<?php

$rootdir = dirname(dirname(__FILE__));

require_once($rootdir . '/htmlfuncs.php');
require_once($rootdir . '/dbfuncs.php');

$result = '';
foreach (db_get_transport_types() as $typeid => $typename) {
    $tableid = "get-transport-table-$typeid";
    $transport = db_get_all_transport_by_type($typeid);
    $table = html_for_table($tableid, 'table table-hover', false, $transport);
    $result .= '<h3>' . $typename . '</h3>' . $table . '<spacer>';
}

echo '<div id="get-transport" class="requestdiv col-lg-12">' . $result . '</div>';
