<?php

$rootdir = dirname(dirname(__FILE__));

require_once($rootdir . '/dbfuncs.php');
require_once($rootdir . '/htmlfuncs.php');

echo '<div id="add-garage-property" class="requestdiv col-lg-12">' .
     html_for_garage_property_type_selection() .
     '<input id="add-garage-property-description" type="text" placeholder="Описание объекта" class="form-control spacer"/>' .
     '<button id="btn-add-garage-property" class="btn btn-primary col-lg-12 spacer">Добавить объект</button>' .
     '</div>';

