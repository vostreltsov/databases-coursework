<?php

$rootdir = dirname(dirname(__FILE__));

require_once($rootdir . '/htmlfuncs.php');
require_once($rootdir . '/dbfuncs.php');

$tableid = "get-garage-property-table";

echo '<div id="get-transport" class="requestdiv col-lg-12">' .
     html_for_table($tableid, 'table table-hover', false, db_get_garage_property()) .
     '</div>';
