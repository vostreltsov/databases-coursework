<?php

$rootdir = dirname(dirname(__FILE__));

require_once($rootdir . '/htmlfuncs.php');

echo '<div id="add-brigadier" class="requestdiv col-lg-6">' .
     '<input id="add-brigadier-name" type="text" placeholder="ФИО" class="form-control spacer"/>' .
     '<input id="add-brigadier-birthdate" type="date" value="1990-12-31" class="form-control col-lg-6 spacer"/>' .
     '<button id="btn-add-brigadier" class="btn btn-primary col-lg-12 spacer">Добавить бригадира</button>' .
     '</div>' .
     "<script type='text/javascript'>" .
     '$("#add-brigadier-birthdate").val(new Date().toJSON().slice(0,10));' .
     "</script>";
