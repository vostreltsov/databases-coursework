<?php

$rootdir = dirname(dirname(__FILE__));

require_once($rootdir . '/htmlfuncs.php');
require_once($rootdir . '/dbfuncs.php');

echo '<div id="get-transportations" class="requestdiv col-lg-12">' .
     '<h3>Транспортное средство</h3>' . html_for_transport_selection(array('Грузовой транспорт')) .
     '<p class="spacer">Начало:</p><input id="get-transportations-start" type="date" class="form-control col-lg-6 spacer"/>' .
     '<p class="spacer">Конец:</p><input id="get-transportations-end" type="date" class="form-control col-lg-6 spacer"/>' .
     '<button id="btn-get-transportations" class="btn btn-primary col-lg-12 spacer">Получить данные о грузоперевозках</button>' .
     '<div id="get-transportations-result">' . '</div>' .
     '</div>' .
     "<script type='text/javascript'>" .
     '$("#get-transportations-start").val(new Date().toJSON().slice(0,10));' .
     '$("#get-transportations-end").val(new Date().toJSON().slice(0,10));' .
     "</script>";
