<?php

$rootdir = dirname(dirname(__FILE__));

require_once($rootdir . '/htmlfuncs.php');
require_once($rootdir . '/dbfuncs.php');

echo '<div id="get-work" class="requestdiv col-lg-12">' .
     '<h3>Транспортное средство</h3>' . html_for_transport_selection() .
     '<button id="btn-get-work" class="btn btn-primary col-lg-12 spacer">Получить данные о ремонтах</button>' .
     '<div id="get-work-result">' . '</div>' .
     '</div>';

