<?php

$rootdir = dirname(dirname(__FILE__));

require_once($rootdir . '/dbfuncs.php');

$types = '<select id="add-transport-type" class="spacer selectpicker width100">';
$attributes = '';
$script = '<script type="text/javascript">
               $("#add-transport-type").change(function(e) {
                   $(".transport-attribute").hide();
                   var typeid = $("#add-transport-type").val().split("-")[2];';

foreach (db_get_transport_types() as $typeid => $typename) {
    $types .= "<option value='add-transport-$typeid'>" . $typename . '</option>';
    $script .= "\n                       if (typeid == $typeid) {\n";
    foreach (db_get_transport_type_attributes($typeid) as $attrid => $attrname) {
        $attrid = "add-transport-$attrid";
        $attributes .= "<input id='$attrid' type='text' placeholder='$attrname' class='form-control transport-attribute spacer'/>";
        $script .= "                           $('#$attrid').show();";
    }
    $script .= "\n                       }";
}
$types .= '</select>';
$script .= "\n});$('#add-transport-type').trigger('change');</script>";

echo '<div id="add-transport" class="requestdiv col-lg-6">' .
     $types .
     $attributes .
     $script .
     '<input id="add-transport-marque" type="text" placeholder="Марка" class="form-control spacer"/>' .
     '<p class="spacer">Дата получения:</p><input id="add-transport-date" type="date" class="form-control col-lg-6 spacer"/>' .
     '<button id="btn-add-transport" class="btn btn-primary col-lg-12 spacer">Добавить транспорт</button>' .
     '</div>' .
     "<script type='text/javascript'>" .
     '$("#add-transport-date").val(new Date().toJSON().slice(0,10));' .
     "</script>";
