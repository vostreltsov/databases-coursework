<?php

$rootdir = dirname(dirname(__FILE__));

require_once($rootdir . '/htmlfuncs.php');

echo '<div id="add-kilometrage" class="requestdiv col-lg-6">' .
     '<h3>Транспортное средство</h3>' . html_for_transport_selection() .
     '<input id="add-kilometrage-distance" type="number" placeholder="Расстояние" class="form-control spacer"/>' .
     '<p class="spacer">Начало:</p><input id="add-kilometrage-start" type="date" class="form-control col-lg-6 spacer"/>' .
     '<p class="spacer">Конец:</p><input id="add-kilometrage-end" type="date" class="form-control col-lg-6 spacer"/>' .
     '<button id="btn-add-kilometrage" class="btn btn-primary col-lg-12 spacer">Добавить пробег</button>' .
     '</div>' .
     "<script type='text/javascript'>" .
     '$("#add-kilometrage-start").val(new Date().toJSON().slice(0,10));' .
     '$("#add-kilometrage-end").val(new Date().toJSON().slice(0,10));' .
     "</script>";
