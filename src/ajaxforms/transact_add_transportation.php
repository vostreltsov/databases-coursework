<?php

$rootdir = dirname(dirname(__FILE__));

require_once($rootdir . '/htmlfuncs.php');

echo '<div id="add-transportation" class="requestdiv col-lg-6">' .
     '<h3>Транспортное средство</h3>' . html_for_transport_selection(array('Грузовой транспорт')) .
     '<input id="add-transportation-description" type="text" placeholder="Описание грузоперевозки" class="form-control spacer"/>' .
     '<input id="add-transportation-distance" type="number" placeholder="Расстояние" class="form-control spacer"/>' .
     '<p class="spacer">Начало:</p><input id="add-transportation-start" type="date" class="form-control col-lg-6 spacer"/>' .
     '<p class="spacer">Конец:</p><input id="add-transportation-end" type="date" class="form-control col-lg-6 spacer"/>' .
     '<button id="btn-add-transportation" class="btn btn-primary col-lg-12 spacer">Добавить грузоперевозку</button>' .
     '</div>' .
     "<script type='text/javascript'>" .
     '$("#add-transportation-start").val(new Date().toJSON().slice(0,10));' .
     '$("#add-transportation-end").val(new Date().toJSON().slice(0,10));' .
     "</script>";
