<?php

$rootdir = dirname(__FILE__);

session_start();

if (!isset($_SESSION['user']) || !isset($_SESSION['password'])){
    header('Location: loginform.php');
    exit();
}

// Is the system installed?
if (!file_exists('config.php')) {
    header("Location: install.php");
    exit();
}


require_once($rootdir . '/dbfuncs.php');
require_once($rootdir . '/htmlfuncs.php');

// Send the main page
header("HTTP/1.1 200 OK");

echo html_header('Автомобильное предприятие');

echo
'
  <div class="navbar navbar-default">
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href class="dropdown-toggle" data-toggle="dropdown">Справочные запросы <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href class="menuitem" id="menu-get-transport">Получить данные об автопарке предприятия</a></li>
            <li><a href class="menuitem" id="menu-get-drivers-by-transport">Получить перечень и общее число водителей по предприятию, по указанной автомашине</a></li>
            <li><a href class="menuitem" id="menu-get-drivers-distribution">Получить распределение водителей по автомобилям</a></li>
            <li><a href class="menuitem" id="menu-get-transport-distribution">Получить распределение пассажирского автотранспорта по маршрутам</a></li>
            <li><a href class="menuitem" id="menu-get-kilometrage">Получить данные о пробеге автотранспорта</a></li>
            <li><a href class="menuitem" id="menu-get-work-by-transport">Получить данные о числе ремонтов и их стоимости для автотранспорта</a></li>
            <li><a href class="menuitem" id="menu-get-garage-property">Получить данные о гаражном хозяйстве</a></li>
            <li><a href class="menuitem" id="menu-get-transportations">Получить данные о грузоперевозках, выполненных указанной автомашиной за обозначенный период</a></li>
            <li><a href class="menuitem" id="menu-get-parts-count">Получить данные о числе использованных для ремонта указанных узлов и агрегатов для транспорта</a></li>
            <li><a href class="menuitem" id="menu-get-adopted-decommissioned-transport">Получить данные о полученной и списанной автотехнике за указанный период</a></li>
            <li><a href class="menuitem" id="menu-get-brigadier-subordinates">Получить состав подчиненных бригадира</a></li>
            <li><a href class="menuitem" id="menu-get-master-subordinates">Получить состав подчиненных мастера</a></li>
            <li><a href class="menuitem" id="menu-get-work-by-worker">Получить данные о работах, выполненных указанным специалистом</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href class="dropdown-toggle" data-toggle="dropdown">Транзакционные запросы <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href class="menuitem" id="menu-add-transport">Добавить транспортное средство</a></li>
            <li><a href class="menuitem" id="menu-remove-transport">Списать транспортное средство</a></li>
            <li class="divider"></li>
            <li><a href class="menuitem" id="menu-add-worker">Добавить сотрудника в обслуживающий персонал</a></li>
            <li><a href class="menuitem" id="menu-remove-worker">Уволить сотрудника обслуживающего персонала</a></li>
            <li class="divider"></li>
            <li><a href class="menuitem" id="menu-add-driver">Добавить водителя</a></li>
            <li><a href class="menuitem" id="menu-remove-driver">Уволить водителя</a></li>
            <li><a href class="menuitem" id="menu-set-driver">Закрепить водителя за транспортом</a></li>
            <li class="divider"></li>
            <li><a href class="menuitem" id="menu-add-route">Добавить маршрут</a></li>
            <li><a href class="menuitem" id="menu-remove-route">Удалить маршрут</a></li>
            <li><a href class="menuitem" id="menu-set-route">Закрепить автомобиль за маршрутом</a></li>
            <li class="divider"></li>
            <li><a href class="menuitem" id="menu-add-brigadier">Добавить бригадира</a></li>
            <li><a href class="menuitem" id="menu-add-master">Добавить мастера</a></li>
            <li><a href class="menuitem" id="menu-set-master-for-brigadier">Задать начальника-мастера для бригадира</a></li>
            <li><a href class="menuitem" id="menu-set-brigadier-for-worker">Задать начальника-бригадира для рабочего</a></li>
            <li class="divider"></li>
            <li><a href class="menuitem" id="menu-add-garage-property">Добавить объект гаражного хозяйства</a></li>
            <li><a href class="menuitem" id="menu-add-transportation">Добавить грузоперевозку</a></li>
            <li><a href class="menuitem" id="menu-add-kilometrage">Добавить пробег к автомобилю</a></li>
            <li><a href class="menuitem" id="menu-add-work">Добавить выполненную работу</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href class="dropdown-toggle" data-toggle="dropdown">Плановые запросы <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href class="menuitem" id="menu-assign-transport">Составить распределение автотранспорта по маршрутам</a></li>
          </ul>
        </li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
        <li><p class="navbar-text">Вы зашли как: ' . $_SESSION['user'] . '</p></li>
        <li><a href="./logout.php" type="submit" name="action" value="logout" method="post">Выйти</a></li>
      </ul>
    </div>
  </div>

  <div class="jumbotron">
    <div id="container" class="container">
    </div>
  </div>
';

echo html_footer();
