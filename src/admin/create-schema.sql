SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `autoenterprise` ;
CREATE SCHEMA IF NOT EXISTS `autoenterprise` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `autoenterprise` ;

-- -----------------------------------------------------
-- Table `autoenterprise`.`worker_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `autoenterprise`.`worker_type` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` TEXT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `autoenterprise`.`worker`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `autoenterprise`.`worker` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` TEXT NOT NULL,
  `birth_date` DATE NOT NULL,
  `worker_type_id` INT(11) NULL,
  `brigadier_id` INT(11) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_worker_brigadier1_idx` (`brigadier_id` ASC),
  CONSTRAINT `worker_ibfk_1`
    FOREIGN KEY (`worker_type_id`)
    REFERENCES `autoenterprise`.`worker_type` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_worker_brigadier1`
    FOREIGN KEY (`brigadier_id`)
    REFERENCES `autoenterprise`.`brigadier` (`id`)
    ON DELETE SET NULL
    ON UPDATE SET NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `autoenterprise`.`master`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `autoenterprise`.`master` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `worker_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `worker_id` (`worker_id` ASC),
  CONSTRAINT `master_ibfk_1`
    FOREIGN KEY (`worker_id`)
    REFERENCES `autoenterprise`.`worker` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `autoenterprise`.`brigadier`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `autoenterprise`.`brigadier` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `worker_id` INT(11) NOT NULL,
  `master_id` INT(11) NULL,
  PRIMARY KEY (`id`),
  INDEX `worker_id` (`worker_id` ASC),
  INDEX `master_id` (`master_id` ASC),
  CONSTRAINT `brigadier_ibfk_1`
    FOREIGN KEY (`worker_id`)
    REFERENCES `autoenterprise`.`worker` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `brigadier_ibfk_2`
    FOREIGN KEY (`master_id`)
    REFERENCES `autoenterprise`.`master` (`id`)
    ON DELETE SET NULL
    ON UPDATE SET NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `autoenterprise`.`transport_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `autoenterprise`.`transport_type` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` TEXT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `autoenterprise`.`route`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `autoenterprise`.`route` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` TEXT NULL,
  `start` TEXT NOT NULL,
  `end` TEXT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `autoenterprise`.`transport`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `autoenterprise`.`transport` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `transport_type_id` INT(11) NOT NULL,
  `route_id` INT(11) NULL DEFAULT NULL,
  `marque` TEXT NOT NULL,
  `date_adopted` DATE NOT NULL,
  `date_decommissioned` DATE NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `transport_ibfk_1`
    FOREIGN KEY (`transport_type_id`)
    REFERENCES `autoenterprise`.`transport_type` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `transport_ibfk_2`
    FOREIGN KEY (`route_id`)
    REFERENCES `autoenterprise`.`route` (`id`)
    ON DELETE SET NULL
    ON UPDATE SET NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `autoenterprise`.`driver`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `autoenterprise`.`driver` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `transport_id` INT(11) NULL DEFAULT NULL,
  `worker_id` INT(11) NOT NULL,
  `category` TEXT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `transport_id` (`transport_id` ASC),
  INDEX `fk_driver_worker1_idx` (`worker_id` ASC),
  CONSTRAINT `driver_ibfk_2`
    FOREIGN KEY (`transport_id`)
    REFERENCES `autoenterprise`.`transport` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_driver_worker1`
    FOREIGN KEY (`worker_id`)
    REFERENCES `autoenterprise`.`worker` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `autoenterprise`.`transport_type_attribute`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `autoenterprise`.`transport_type_attribute` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` TEXT NOT NULL,
  `transport_type_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `transport_type_attribute_ibfk_1`
    FOREIGN KEY (`transport_type_id`)
    REFERENCES `autoenterprise`.`transport_type` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `autoenterprise`.`transport_attribute`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `autoenterprise`.`transport_attribute` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `value` TEXT NOT NULL,
  `transport_type_attribute_id` INT(11) NOT NULL,
  `transport_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `transport_attribute_ibfk_1`
    FOREIGN KEY (`transport_type_attribute_id`)
    REFERENCES `autoenterprise`.`transport_type_attribute` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `transport_attribute_ibfk_2`
    FOREIGN KEY (`transport_id`)
    REFERENCES `autoenterprise`.`transport` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `autoenterprise`.`transport_part_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `autoenterprise`.`transport_part_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` TEXT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `autoenterprise`.`work`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `autoenterprise`.`work` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `description` TEXT NOT NULL,
  `date_start` DATE NOT NULL,
  `date_end` DATE NOT NULL,
  `cost` INT NOT NULL,
  `worker_id` INT(11) NOT NULL,
  `transport_part_type_id` INT NOT NULL,
  `transport_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_work_transport_part_type1_idx` (`transport_part_type_id` ASC),
  INDEX `fk_work_transport1_idx` (`transport_id` ASC),
  CONSTRAINT `work_ibfk_1`
    FOREIGN KEY (`worker_id`)
    REFERENCES `autoenterprise`.`worker` (`id`),
  CONSTRAINT `fk_work_transport_part_type1`
    FOREIGN KEY (`transport_part_type_id`)
    REFERENCES `autoenterprise`.`transport_part_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_work_transport1`
    FOREIGN KEY (`transport_id`)
    REFERENCES `autoenterprise`.`transport` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `autoenterprise`.`worker_type_attribute`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `autoenterprise`.`worker_type_attribute` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` TEXT NOT NULL,
  `worker_type_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `worker_type_attribute_ibfk_1`
    FOREIGN KEY (`worker_type_id`)
    REFERENCES `autoenterprise`.`worker_type` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `autoenterprise`.`worker_attribute`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `autoenterprise`.`worker_attribute` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `value` TEXT NOT NULL,
  `worker_type_attribute_id` INT(11) NOT NULL,
  `worker_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `worker_attribute_ibfk_1`
    FOREIGN KEY (`worker_type_attribute_id`)
    REFERENCES `autoenterprise`.`worker_type_attribute` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `worker_attribute_ibfk_2`
    FOREIGN KEY (`worker_id`)
    REFERENCES `autoenterprise`.`worker` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `autoenterprise`.`kilometrage`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `autoenterprise`.`kilometrage` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `transport_id` INT(11) NOT NULL,
  `distance` INT NOT NULL,
  `date_start` DATE NOT NULL,
  `date_end` DATE NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_kilometrage_transport1_idx` (`transport_id` ASC),
  CONSTRAINT `fk_kilometrage_transport1`
    FOREIGN KEY (`transport_id`)
    REFERENCES `autoenterprise`.`transport` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `autoenterprise`.`transportation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `autoenterprise`.`transportation` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` TEXT NULL,
  `kilometrage_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_freight_transportation_kilometrage1_idx` (`kilometrage_id` ASC),
  CONSTRAINT `fk_freight_transportation_kilometrage1`
    FOREIGN KEY (`kilometrage_id`)
    REFERENCES `autoenterprise`.`kilometrage` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `autoenterprise`.`garage_property_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `autoenterprise`.`garage_property_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `autoenterprise`.`garage_property`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `autoenterprise`.`garage_property` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(45) NULL,
  `garage_property_type_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_garage_property_garage_property_type1_idx` (`garage_property_type_id` ASC),
  CONSTRAINT `fk_garage_property_garage_property_type1`
    FOREIGN KEY (`garage_property_type_id`)
    REFERENCES `autoenterprise`.`garage_property_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
