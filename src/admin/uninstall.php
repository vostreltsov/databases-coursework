<?php

$rootdir = dirname(dirname(__FILE__));

if (!file_exists($rootdir . '/config.php')) {
    echo "Looks like nothing is installed\n";
    exit();
}

require_once($rootdir . '/config.php');
global $CFG;

$mysqli = new mysqli('localhost', $CFG->dbuser, $CFG->dbpassword);
if ($mysqli->connect_errno) {
    echo 'Failed to connect to MySQL: ' . $mysqli->connect_errno . "\n";
    exit();
}

$queries = array("DROP DATABASE IF EXISTS {$CFG->dbname};");

foreach ($queries as $query) {
    if (!$mysqli->query($query)) {
        echo 'Could not execute query: ' . $query . "\n";
        exit();
    }
}

unlink($rootdir . '/config.php');

echo "Done!\n";
