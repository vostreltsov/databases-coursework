<?php

$rootdir = dirname(dirname(__FILE__));

if (!isset($_POST['dbuser']) || !isset($_POST['dbpassword'])) {
    echo "invalid POST request\n";
    exit();
}

$user = $_POST['dbuser'];
$password = $_POST['dbpassword'];
$dbname = 'autoenterprise';

// Create the database and tables
$mysqli = new mysqli('localhost', $user, $password);
if ($mysqli->connect_errno) {
    echo 'Failed to connect to MySQL: ' . $mysqli->connect_errno . "\n";
    exit();
}

$query = file_get_contents('create-schema.sql');
if ($mysqli->multi_query($query)) {
    while ($mysqli->next_result()) {
    }
}

// INSERT SOME PRE-DEFINED ROWS

$queries = array(
"USE $dbname",

// Transport-related tables
"INSERT INTO transport_part_type(name) VALUES('Двигатель')",
"INSERT INTO transport_part_type(name) VALUES('Подвеска')",
"INSERT INTO transport_part_type(name) VALUES('Кузов')",

"INSERT INTO transport_type(name) VALUES('Автобус')",
"INSERT INTO transport_type(name) VALUES('Такси')",
"INSERT INTO transport_type(name) VALUES('Маршрутное такси')",
"INSERT INTO transport_type(name) VALUES('Грузовой транспорт')",
"INSERT INTO transport_type(name) VALUES('Вспомогательный транспорт')",
"INSERT INTO transport_type(name) VALUES('Прочий легковой транспорт')",

"INSERT INTO transport_type_attribute(name, transport_type_id) SELECT 'Вместимость', id FROM transport_type WHERE name = 'Автобус'",
"INSERT INTO transport_type_attribute(name, transport_type_id) SELECT 'Комфортабельность', id FROM transport_type WHERE name = 'Такси'",
"INSERT INTO transport_type_attribute(name, transport_type_id) SELECT 'Вместимость', id FROM transport_type WHERE name = 'Маршрутное такси'",
"INSERT INTO transport_type_attribute(name, transport_type_id) SELECT 'Грузоподъемность', id FROM transport_type WHERE name = 'Грузовой транспорт'",
"INSERT INTO transport_type_attribute(name, transport_type_id) SELECT 'Назначение', id FROM transport_type WHERE name = 'Вспомогательный транспорт'",
"INSERT INTO transport_type_attribute(name, transport_type_id) SELECT 'Время разгона до 100 км/ч', id FROM transport_type WHERE name = 'Прочий легковой транспорт'",

// Worker-related tables
"INSERT INTO worker_type(name) VALUES('Техник')",
"INSERT INTO worker_type(name) VALUES('Сварщик')",
"INSERT INTO worker_type(name) VALUES('Слесарь')",
"INSERT INTO worker_type(name) VALUES('Сборщик')",

"INSERT INTO worker_type_attribute(name, worker_type_id) SELECT 'Квалификация', id FROM worker_type WHERE name = 'Техник'",
"INSERT INTO worker_type_attribute(name, worker_type_id) SELECT 'Разряд', id FROM worker_type WHERE name = 'Сварщик'",
"INSERT INTO worker_type_attribute(name, worker_type_id) SELECT 'Разряд', id FROM worker_type WHERE name = 'Слесарь'",
"INSERT INTO worker_type_attribute(name, worker_type_id) SELECT 'Квалификация', id FROM worker_type WHERE name = 'Сборщик'",

// Garage property-related tables
"INSERT INTO garage_property_type(name) VALUES('Цех')",
"INSERT INTO garage_property_type(name) VALUES('Гараж')",
"INSERT INTO garage_property_type(name) VALUES('Бокс')",

// Stored procedures
"CREATE PROCEDURE ASSIGN_TRANSPORT_TO_ROUTES
(IN transport_type_id INT)
proc_label: BEGIN

    DECLARE transport_count INT DEFAULT 0;
    DECLARE route_count INT DEFAULT 0;
    DECLARE transport_per_route INT DEFAULT 0;
    DECLARE inserted INT DEFAULT 0;
    DECLARE transport_id, route_id INT;

    DECLARE cur_transport CURSOR FOR SELECT id FROM transport WHERE date_decommissioned IS NULL AND transport.transport_type_id = transport_type_id;
    DECLARE cur_route CURSOR FOR SELECT id FROM route;

    SELECT count(*) FROM transport WHERE transport.transport_type_id = transport_type_id INTO transport_count;
    SELECT count(*) FROM route INTO route_count;

    IF transport_count = 0 OR route_count = 0 THEN
        LEAVE proc_label;
    END IF;

    SET transport_per_route = 1;
    WHILE transport_per_route * route_count < transport_count DO
        SET transport_per_route = transport_per_route + 1;
    END WHILE;

    OPEN cur_transport;
    OPEN cur_route;

    WHILE inserted < transport_count DO
        FETCH cur_transport INTO transport_id;

        IF inserted % transport_per_route = 0 THEN
            FETCH cur_route INTO route_id;
        end if;

        UPDATE transport SET transport.route_id = route_id WHERE transport.id = transport_id;
        SET inserted = inserted + 1;
    END WHILE;

    CLOSE cur_transport;
    CLOSE cur_route;
END",

// Triggers
"CREATE TRIGGER transport_AUPD AFTER UPDATE ON transport FOR EACH ROW
UPDATE driver SET transport_id = NULL WHERE transport_id = NEW.id;",

"CREATE TRIGGER transport_attribute_BINS BEFORE INSERT ON transport_attribute FOR EACH ROW
BEGIN
    DECLARE transport_type_id INT;
    DECLARE map_count INT DEFAULT 0;
    DECLARE attr_count INT DEFAULT 0;

    SELECT count(*) FROM transport_attribute
    WHERE transport_attribute.transport_type_attribute_id = NEW.transport_type_attribute_id AND transport_attribute.transport_id = NEW.transport_id
    INTO attr_count;

    IF attr_count > 0 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'This attribute is already set for this transport';
    END IF;

    SELECT transport.transport_type_id FROM transport WHERE transport.id = NEW.transport_id INTO transport_type_id;

    SELECT count(*) FROM transport_type_attribute
    WHERE transport_type_attribute.id = NEW.transport_type_attribute_id AND transport_type_attribute.transport_type_id = transport_type_id
    INTO map_count;

    IF map_count = 0 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Inappropriate attribute for this transport type';
    END IF;
END",

"CREATE TRIGGER worker_attribute_BINS BEFORE INSERT ON worker_attribute FOR EACH ROW
BEGIN
    DECLARE worker_type_id INT;
    DECLARE map_count INT DEFAULT 0;
    DECLARE attr_count INT DEFAULT 0;

    SELECT count(*) FROM worker_attribute
    WHERE worker_attribute.worker_type_attribute_id = NEW.worker_type_attribute_id AND worker_attribute.worker_id = NEW.worker_id
    INTO attr_count;

    IF attr_count > 0 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'This attribute is already set for this worker';
    END IF;

    SELECT worker.worker_type_id FROM worker WHERE worker.id = NEW.worker_id INTO worker_type_id;

    SELECT count(*) FROM worker_type_attribute
    WHERE worker_type_attribute.id = NEW.worker_type_attribute_id AND worker_type_attribute.worker_type_id = worker_type_id
    INTO map_count;

    IF map_count = 0 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Inappropriate attribute for this worker type';
    END IF;
END",

// View for drivers distribution
"CREATE VIEW drivers_distribution AS
SELECT driver.id AS 'driver_id', worker.name, worker.birth_date, driver.category, transport_type.name AS 'transport_type_name', transport.marque FROM
(driver INNER JOIN worker ON worker.id = driver.worker_id
        INNER JOIN transport ON transport.id = driver.transport_id
        INNER JOIN transport_type on transport_type.id = transport.transport_type_id);",

// View for transport distribution
"CREATE VIEW transport_distribution AS
SELECT transport.id AS 'transport_id', transport_type.name AS 'transport_type_name', transport.marque, route.name AS 'route_name' FROM
(transport INNER JOIN transport_type ON transport_type.id = transport.transport_type_id AND (transport_type.name = 'Автобус' OR transport_type.name = 'Маршрутное такси')
           INNER JOIN route ON route.id = transport.route_id);",

// View for work
"CREATE VIEW detailed_work AS
SELECT transport.id AS 'transport_id', worker.id AS 'worker_id', transport_type.name AS 'transport_type_name', transport.marque, transport_part_type.name AS 'transport_part_type_name', cost FROM
(transport INNER JOIN transport_type ON transport_type.id = transport.transport_type_id
           INNER JOIN work ON transport.id = work.transport_id
           INNER JOIN transport_part_type ON transport_part_type.id = work.transport_part_type_id
           INNER JOIN worker ON work.worker_id = worker.id);",

// View for transportations
"CREATE VIEW detailed_transportations AS
SELECT transportation.id, transport.id AS 'transport_id', description, date_start, date_end, distance FROM
(transportation INNER JOIN kilometrage ON kilometrage.id = transportation.kilometrage_id
                INNER JOIN transport ON kilometrage.transport_id = transport.id);",

// View for garage property
"CREATE VIEW detailed_garage_property AS
SELECT garage_property.id, garage_property_type.name, garage_property.description FROM
(garage_property INNER JOIN garage_property_type ON garage_property.garage_property_type_id = garage_property_type.id);",

// All privileges go to admin
"GRANT USAGE ON *.* TO 'admin'@'localhost'",
"DROP USER 'admin'@'localhost'",
"CREATE USER 'admin'@'localhost' IDENTIFIED BY 'admin'",
"UPDATE mysql.user SET Password=PASSWORD('admin') WHERE User='admin'",
"GRANT ALL PRIVILEGES ON autoenterprise.* to admin WITH GRANT OPTION",

// HR privileges
"GRANT USAGE ON *.* TO 'hr'@'localhost'",
"DROP USER 'hr'@'localhost'",
"CREATE USER 'hr'@'localhost' IDENTIFIED BY 'hr'",
"UPDATE mysql.user SET Password=PASSWORD('hr') WHERE User='hr'",
"GRANT ALL PRIVILEGES ON worker_type TO hr",
"GRANT ALL PRIVILEGES ON worker_attribute TO hr",
"GRANT ALL PRIVILEGES ON worker_type_attribute TO hr",
"GRANT ALL PRIVILEGES ON worker TO hr",
"GRANT ALL PRIVILEGES ON driver TO hr",
"GRANT ALL PRIVILEGES ON brigadier TO hr",
"GRANT ALL PRIVILEGES ON master TO hr",
"GRANT ALL PRIVILEGES ON work TO hr",
"GRANT SELECT ON transport_type TO hr",
"GRANT SELECT ON transport_attribute TO hr",
"GRANT SELECT ON transport_type_attribute TO hr",
"GRANT SELECT ON transport TO hr",
"GRANT SELECT ON drivers_distribution TO hd",
"GRANT SELECT ON detailed_work TO hd",

// Logistics department priveleges
"GRANT USAGE ON *.* TO 'ld'@'localhost'",
"DROP USER 'ld'@'localhost'",
"CREATE USER 'ld'@'localhost' IDENTIFIED BY 'ld'",
"UPDATE mysql.user SET Password=PASSWORD('ld') WHERE User='ld'",
"GRANT ALL PRIVILEGES ON transport_type TO ld",
"GRANT ALL PRIVILEGES ON transport_attribute TO ld",
"GRANT ALL PRIVILEGES ON transport_type_attribute TO ld",
"GRANT ALL PRIVILEGES ON transport TO ld",
"GRANT ALL PRIVILEGES ON route TO ld",
"GRANT ALL PRIVILEGES ON kilometrage TO ld",
"GRANT ALL PRIVILEGES ON transportation TO ld",
"GRANT ALL PRIVILEGES ON garage_property_type TO ld",
"GRANT ALL PRIVILEGES ON garage_property TO ld",
"GRANT EXECUTE ON PROCEDURE ASSIGN_TRANSPORT_TO_ROUTES TO ld",
"GRANT SELECT ON worker_type TO ld",
"GRANT SELECT ON worker_attribute TO ld",
"GRANT SELECT ON worker_type_attribute TO ld",
"GRANT SELECT ON worker TO ld",
"GRANT SELECT ON brigadier TO ld",
"GRANT SELECT ON master TO ld",
"GRANT SELECT ON work TO ld",
"GRANT SELECT ON drivers_distribution TO ld",
"GRANT SELECT ON transport_distribution TO ld",
"GRANT SELECT ON detailed_transportations TO ld",
"GRANT SELECT ON detailed_garage_property TO ld",

// Technical department priveleges
"GRANT USAGE ON *.* TO 'td'@'localhost'",
"DROP USER 'td'@'localhost'",
"CREATE USER 'td'@'localhost' IDENTIFIED BY 'td'",
"UPDATE mysql.user SET Password=PASSWORD('td') WHERE User='td'",
"GRANT ALL PRIVILEGES ON transport_type TO td",
"GRANT ALL PRIVILEGES ON transport_attribute TO td",
"GRANT ALL PRIVILEGES ON transport_type_attribute TO td",
"GRANT ALL PRIVILEGES ON transport TO td",
"GRANT SELECT ON route TO td",
"GRANT SELECT ON kilometrage TO td",
"GRANT SELECT ON transportation TO td",
"GRANT SELECT ON garage_property_type TO td",
"GRANT SELECT ON garage_property TO td",
"GRANT SELECT ON worker_type TO td",
"GRANT SELECT ON worker_attribute TO td",
"GRANT SELECT ON worker_type_attribute TO td",
"GRANT SELECT ON worker TO td",
"GRANT SELECT ON brigadier TO td",
"GRANT SELECT ON master TO td",
"GRANT SELECT ON work TO td",
"GRANT SELECT ON detailed_work TO td",
"GRANT SELECT ON detailed_garage_property TO td",

"FLUSH PRIVILEGES"
);

foreach ($queries as $query) {
    if (!$mysqli->query($query)) {
        echo 'Could not execute query: ' . $query . "\n";
        echo $mysqli->error;
        exit();
    }
}

// OK, write the config file
$cfg = '<?php' . "\n" .
       'unset($CFG);' . "\n" .
       'global $CFG;' . "\n" .
       '$CFG = new stdClass();' . "\n" .
       '$CFG->dbuser = ' . "'$user';\n" .
       '$CFG->dbpassword = ' . "'$password';\n" .
       '$CFG->dbname = ' . "'$dbname';\n";

if (file_put_contents($rootdir . '/config.php', $cfg) === false) {
    echo "directory $rootdir not writeable\n";
    exit();
}

// Redirect to index.php
$destination = $_SERVER['SERVER_NAME'] . '/' . basename($rootdir);
header("Location: http://{$destination}/");
