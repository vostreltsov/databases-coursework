<?php

$rootdir = dirname(__FILE__);
require_once($rootdir . '/config.php');

session_start();

function db_connect() {
    global $CFG;
    $mysqli = new mysqli('localhost', $_SESSION['user'], $_SESSION['password'], $CFG->dbname);
    if ($mysqli->connect_errno) {
        exit('Could not connect to the database');
    }
    return $mysqli;
}

function db_ajax_query($mysqli, $query) {
    $result = $mysqli->query($query);
    if (!$result) {
        echo 'Could not execute query: ' . $mysqli->error;
        exit();
    }
    return $result;
}

/**
 * Returns all transport types in the format "id => transport type name".
 */
function db_get_transport_types() {
    $mysqli = db_connect();
    $rows = $mysqli->query('SELECT * FROM transport_type');
    $result = array();
    if (!$rows) {
        return $result;
    }
    while ($row = $rows->fetch_array(MYSQLI_ASSOC)) {
        $result[$row['id']] = $row['name'];
    }
    return $result;
}

/**
 * Returns all transport part types in the format "id => transport part name".
 */
function db_get_transport_part_types() {
    $mysqli = db_connect();
    $rows = $mysqli->query('SELECT * FROM transport_part_type');
    $result = array();
    if (!$rows) {
        return $result;
    }
    while ($row = $rows->fetch_array(MYSQLI_ASSOC)) {
        $result[$row['id']] = $row['name'];
    }
    return $result;
}

/**
 * Returns all attributes for the specified transport type in the format "id => attribute name".
 */
function db_get_transport_type_attributes($typeid) {
    $mysqli = db_connect();
    $rows = $mysqli->query("SELECT * FROM transport_type_attribute WHERE transport_type_id = $typeid");
    $result = array();
    if (!$rows) {
        return $result;
    }
    while ($row = $rows->fetch_array(MYSQLI_ASSOC)) {
        $result[$row['id']] = $row['name'];
    }
    return $result;
}

function db_get_worker_types() {
    $mysqli = db_connect();
    $rows = $mysqli->query('SELECT * FROM worker_type');
    $result = array();
    if (!$rows) {
        return $result;
    }
    while ($row = $rows->fetch_array(MYSQLI_ASSOC)) {
        $result[$row['id']] = $row['name'];
    }
    return $result;
}

/**
 * Returns all garage property types in the format "id => garage property type name".
 */
function db_get_garage_property_types() {
    $mysqli = db_connect();
    $rows = $mysqli->query('SELECT * FROM garage_property_type');
    $result = array();
    if (!$rows) {
        return $result;
    }
    while ($row = $rows->fetch_array(MYSQLI_ASSOC)) {
        $result[$row['id']] = $row['name'];
    }
    return $result;
}

/**
 * Returns all attributes for the specified worker type in the format "id => attribute name".
 */
function db_get_worker_type_attributes($typeid) {
    $mysqli = db_connect();
    $rows = $mysqli->query("SELECT * FROM worker_type_attribute WHERE worker_type_id = $typeid");
    $result = array();
    if (!$rows) {
        return $result;
    }
    while ($row = $rows->fetch_array(MYSQLI_ASSOC)) {
        $result[$row['id']] = $row['name'];
    }
    return $result;
}

/**
 * Returns all transport of the given type; 1st row is 'header' for html usage
 * Format is: id, route, attribute1, ..., attributeN
 */
function db_get_all_transport_by_type($typeid, $dateadopted = null, $datedecommissioned = null) {
    $mysqli = db_connect();
    $result = array();
    $attributes = db_get_transport_type_attributes($typeid);

    $header = array('id', 'Марка', 'Маршрут');
    foreach ($attributes as $attributename) {
        $header[] = $attributename;
    }
    $result[] = $header;

    $query = "SELECT * FROM transport WHERE transport_type_id = $typeid";
    if ($dateadopted !== null) {
        $start = $dateadopted['start'];
        $end = $dateadopted['end'];
        $query .= " AND date_adopted >= '$start' AND date_adopted <= '$end'";
    } else if ($datedecommissioned !== null) {
        $start = $datedecommissioned['start'];
        $end = $datedecommissioned['end'];
        $query .= " AND date_decommissioned >= '$start' AND date_decommissioned <= '$end'";
    } else {
        $query .= " AND date_decommissioned IS NULL";
    }
    $rows = $mysqli->query($query);
    if (!$rows) {
        return $result;
    }

    while ($row = $rows->fetch_array(MYSQLI_ASSOC)) {
        $transportid = $row['id'];
        $routeid = $row['route_id'];
        $routename = $mysqli->query("SELECT name FROM route WHERE id = $routeid");
        if ($routename) {
            $routename = $routename->fetch_array(MYSQLI_ASSOC);
            $routename = $routename['name'];
        } else {
            $routename = 'NULL';
        }
        $toadd = array();
        $toadd[] = $transportid;
        $toadd[] = $row['marque'];
        $toadd[] = $routename;
        foreach ($attributes as $attrid => $attributename) {
            $value = $mysqli->query("SELECT * FROM transport_attribute WHERE transport_type_attribute_id = $attrid AND transport_id = $transportid");
            if ($value) {
                $value = $value->fetch_array(MYSQLI_ASSOC);
                $toadd[$attributename] = $value['value'];
            } else {
                $toadd[$attributename] = 'NULL';
            }
        }
        $result[$transportid] = $toadd;
    }
    return $result;
}

/**
 * Returns all workers of the given type; 1st row is 'header' for html usage
 * Format is: id, name, birthday, attribute1, ..., attributeN
 */
function db_get_all_workers($typeid) {
    $mysqli = db_connect();
    $result = array();
    $attributes = db_get_worker_type_attributes($typeid);

    $header = array('id', 'Имя', 'Дата рождения');
    foreach ($attributes as $attributename) {
        $header[] = $attributename;
    }
    $result[] = $header;

    $rows = $mysqli->query("SELECT * FROM worker WHERE worker_type_id = $typeid");
    if (!$rows) {
        return $result;
    }

    while ($row = $rows->fetch_array(MYSQLI_ASSOC)) {
        $workerid = $row['id'];
        $toadd = array();
        $toadd[] = $workerid;
        $toadd[] = $row['name'];
        $toadd[] = $row['birth_date'];
        foreach ($attributes as $attrid => $attributename) {
            $value = $mysqli->query("SELECT * FROM worker_attribute WHERE worker_type_attribute_id = $attrid AND worker_id = $workerid");
            if ($value) {
                $value = $value->fetch_array(MYSQLI_ASSOC);
                $toadd[$attributename] = $value['value'];
            } else {
                $toadd[$attributename] = 'NULL';
            }
        }
        $result[$workerid] = $toadd;
    }
    return $result;
}

/**
 * Returns all drivers; 1st row is 'header' for html usage
 * Format is: id, name, birthday, category
 */
function db_get_all_drivers($transportid = -1) {
    $mysqli = db_connect();
    $result = array();
    $result[] = array('id', 'Имя', 'Дата рождения', 'Категория');

    $query = "SELECT driver.id, name, birth_date, category FROM
              (driver INNER JOIN worker ON worker_id = worker.id)";
    if ($transportid != -1) {
        $query .= " WHERE transport_id = $transportid";
    }

    $rows = $mysqli->query($query);

    if (!$rows) {
        return $result;
    }

    while ($row = $rows->fetch_array(MYSQLI_NUM)) {
        $result[] = $row;
    }
    return $result;
}

function db_get_all_brigadiers() {
    $mysqli = db_connect();
    $result = array();
    $result[] = array('id', 'Имя', 'Дата рождения');

    $rows = $mysqli->query("SELECT brigadier.id, name, birth_date FROM (brigadier INNER JOIN worker ON worker_id = worker.id)");

    if (!$rows) {
        return $result;
    }

    while ($row = $rows->fetch_array(MYSQLI_NUM)) {
        $result[] = $row;
    }
    return $result;
}

function db_get_all_masters() {
    $mysqli = db_connect();
    $result = array();
    $result[] = array('id', 'Имя', 'Дата рождения');

    $rows = $mysqli->query("SELECT master.id, name, birth_date FROM (master INNER JOIN worker ON worker_id = worker.id)");

    if (!$rows) {
        return $result;
    }

    while ($row = $rows->fetch_array(MYSQLI_NUM)) {
        $result[] = $row;
    }
    return $result;
}

function db_get_all_workers_and_drivers() {
    $mysqli = db_connect();
    $result = array();
    $result[] = array('id', 'Имя', 'Дата рождения');

    $rows = $mysqli->query("SELECT worker.id, worker.name, worker.birth_date FROM worker
                            LEFT JOIN brigadier ON worker.id = brigadier.worker_id
                            LEFT JOIN master ON worker.id = master.worker_id
                            WHERE brigadier.worker_id IS NULL AND master.worker_id IS NULL;");

    if (!$rows) {
        return $result;
    }

    while ($row = $rows->fetch_array(MYSQLI_NUM)) {
        $result[] = $row;
    }
    return $result;
}

function db_get_brigadier_subordinates($brigadierid) {
    $mysqli = db_connect();
    $result = array();
    $result[] = array('id', 'Имя', 'Дата рождения');

    $rows = $mysqli->query("SELECT id, name, birth_date FROM worker WHERE brigadier_id = $brigadierid;");

    if (!$rows) {
        return $result;
    }

    while ($row = $rows->fetch_array(MYSQLI_NUM)) {
        $result[] = $row;
    }
    return $result;
}

function db_get_master_subordinates($masterid) {
    $mysqli = db_connect();
    $result = array();
    $result[] = array('id', 'Имя', 'Дата рождения');

    $rows = $mysqli->query("SELECT worker.id, worker.name, worker.birth_date FROM worker
                            INNER JOIN brigadier ON brigadier.worker_id = worker.id
                            WHERE brigadier.master_id = $masterid;");

    if (!$rows) {
        return $result;
    }

    while ($row = $rows->fetch_array(MYSQLI_NUM)) {
        $result[] = $row;
    }
    return $result;
}

function db_get_drivers_distribution() {
    $mysqli = db_connect();
    $result = array();
    $result[] = array('id', 'Имя', 'Дата рождения', 'Категория', 'Транспорт', 'Марка');

    $rows = $mysqli->query("SELECT * FROM drivers_distribution");

    if (!$rows) {
        return $result;
    }

    while ($row = $rows->fetch_array(MYSQLI_NUM)) {
        $result[] = $row;
    }
    return $result;
}

function db_get_transport_distribution() {
    $mysqli = db_connect();
    $result = array();
    $result[] = array('id', 'Транспорт', 'Марка', 'Маршрут');

    $rows = $mysqli->query("SELECT * FROM transport_distribution");

    if (!$rows) {
        return $result;
    }

    while ($row = $rows->fetch_array(MYSQLI_NUM)) {
        $result[] = $row;
    }
    return $result;
}

function db_get_work_by_transport($transportid) {
    $mysqli = db_connect();
    $result = array();
    $result[] = array('transport_id', 'worker_id', 'Транспорт', 'Марка', 'Деталь', 'Цена');

    $rows = $mysqli->query("SELECT * FROM detailed_work WHERE transport_id = $transportid");

    if (!$rows) {
        return $result;
    }

    while ($row = $rows->fetch_array(MYSQLI_NUM)) {
        $result[] = $row;
    }
    return $result;
}

function db_get_work_by_worker($workerid) {
    $mysqli = db_connect();
    $result = array();
    $result[] = array('transport_id', 'worker_id', 'Транспорт', 'Марка', 'Деталь', 'Цена');

    $rows = $mysqli->query("SELECT * FROM detailed_work WHERE worker_id = $workerid");

    if (!$rows) {
        return $result;
    }

    while ($row = $rows->fetch_array(MYSQLI_NUM)) {
        $result[] = $row;
    }
    return $result;
}

function db_get_transportations($transportid, $start, $end) {
    $mysqli = db_connect();
    $result = array();
    $result[] = array('id', 'transport_id', 'Описание', 'Дата начала', 'Дата конца', 'Расстояние');

    $query = "SELECT * FROM detailed_transportations WHERE transport_id = $transportid AND date_start >= '$start' AND date_end <= '$end'";

    $rows = $mysqli->query($query);

    if (!$rows) {
        return $result;
    }

    while ($row = $rows->fetch_array(MYSQLI_NUM)) {
        $result[] = $row;
    }
    return $result;
}

function db_get_garage_property() {
    $mysqli = db_connect();
    $result = array();
    $result[] = array('id', 'Тип', 'Описание');

    $rows = $mysqli->query("SELECT * FROM detailed_garage_property");

    if (!$rows) {
        return $result;
    }

    while ($row = $rows->fetch_array(MYSQLI_NUM)) {
        $result[] = $row;
    }
    return $result;
}

/**
 * Returns all drivers; 1st row is 'header' for html usage
 * Format is: id, name, birthday, category
 */
function db_get_all_routes() {
    $mysqli = db_connect();
    $result = array();
    $result[] = array('id', 'Название', 'Начальная точка', 'Конечная точка');

    $rows = $mysqli->query("SELECT * FROM route");
    if (!$rows) {
        return $result;
    }

    while ($row = $rows->fetch_array(MYSQLI_NUM)) {
        $result[] = $row;
    }
    return $result;
}
