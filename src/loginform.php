<?php

$rootdir = dirname(__FILE__);
require_once($rootdir . '/htmlfuncs.php');

session_start();
if (isset($_SESSION['user']) && isset($_SESSION['password'])) {
    header('Location: index.php');
    exit();
}

echo html_header('Вход в систему');

echo
'<div class="container">
  <form class="col-lg-4" action="login.php" method="post">
    <div class="form-group">
      <input name="user" type="text" placeholder="MySQL user" class="form-control">
    </div>
    <div class="form-group">
      <input name="password" type="password" placeholder="MySQL password" class="form-control">
    </div>
    <button type="submit" class="btn btn-primary col-lg-12">Login</button>
  </form>
</div>';

echo html_footer();
