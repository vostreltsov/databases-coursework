<?php

$rootdir = dirname(__FILE__);
require_once($rootdir . '/htmlfuncs.php');

// Is the system installed?
if (file_exists('config.php')) {
    header("Location: index.php");
    exit();
}

header('Content-Type: text/html; charset=utf-8');
echo html_header('Установка');

echo
'<div class="container">
  <form class="col-lg-4" action="admin/install.php" method="post">
    <div class="form-group">
      <input name="dbuser" type="text" placeholder="MySQL root user" class="form-control">
    </div>
    <div class="form-group">
      <input name="dbpassword" type="password" placeholder="MySQL root password" class="form-control">
    </div>
    <button type="submit" class="btn btn-primary col-lg-12">Install</button>
  </form>
</div>';

echo html_footer();
